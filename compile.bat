if not exist bin mkdir bin
if not exist tmp mkdir tmp
if not exist progs mkdir progs
scalac -classpath "./lib/jar/jep.jar;./lib/jar/akka-actor.jar;./lib/jar/Tsk_DataModel.jar;./lib/jar/sqlite-jdbc-3.8.11.jar;./lib/jar/c3p0-0.9.5.2.jar;./lib/jar/mchange-commons-java-0.2.11.jar" -optimize -explaintypes -deprecation -unchecked -nowarn -d bin  src/prolog/*.scala src/prolog/acts/*.scala src/prolog/builtins/*.scala src/prolog/fluents/*.scala src/prolog/forensic/*.scala src/prolog/interp/*.scala src/prolog/io/*.scala src/prolog/terms/*.scala src/prolog/tests/*.scala
