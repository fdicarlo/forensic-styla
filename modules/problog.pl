module(problog,[problog,lfi,marg,show_result]).

% This module encapsulates problog functionality, which is accessible via python()
%
% Make sure that your python is properly configured. I.e. it has 
% JEP module installed and the appropriate jep.jar file is added to your 
% DFIRE Forensixc Prolog lib/jar folder as per instructions in 
% "Configuring Python for use with DFIRE Forensic Prolog.txt" document.
%
% To use problog, make sure that your installation of python
% also has numpy and problog packages installed:
%
% python -m pip install numpy
% python -m pip install problog
%

:- python('

from problog import get_evaluatable
from problog.program import PrologFile
from problog.program import PrologString
from problog.learning import lfi
from problog.logic import Term

').

% Load ProbLog model specified in the file Model and calculate any queries embedded in it.
% store the resulting probability distribution into Result
problog(Model,Result) :-
  pyset(modelFile,Model),
  python('program=PrologFile(modelFile);result = get_evaluatable().create_from(program).evaluate()'),
  pyget(result,Result).

% Display problog probability distribution in a table, but do not show any rows with 0
% probability.
show_result(Result) :-
  findall([R,P],(member(R-P,Result),P \== 0.0),L),
  table('',['Answer','Probability'],L).

% Learning from examples (LFI) 

:- tskscalafun('problog_examples',2,'     
import prolog.forensic._

def evidence2str(evidence : Cons) : String =
     Cons.to_string(
      {x => x match 
        {
          case l : Cons => evidence2str(l);
          case s : Fun => 
          "(Term.from_string(\\\""+s.getArg(0).toString+".\\\"),"+
          (
            s.getArg(1).asInstanceOf[Const].name match 
            {
              case "true" => "True";
              case "false" => "False";
            }
          )+")";
          case _ => x.toString 
        }
      },
      evidence)

val name:String = getArg(0).asInstanceOf[Const].name;  // Python variable name to hold examples
val evidence:Cons = getArg(1).asInstanceOf[Cons];   // List of lists of pairs (term,true/false).
TSKPython.eval(name+"=eval(\""+evidence2str(evidence)+"\")");
1

').

lfi(Model,Examples,NewModel) :-
  pyset(model,Model),
  problog_examples(examples,Examples),
  python('program=PrologString(model);score, weights, atoms, iteration, lfi_problem = lfi.run_lfi(program,examples);newModel=lfi_problem.get_model()'),
  pyget(newModel,NewModel).

marg(Model,Result) :-
  pyset(model,Model), 
  python('prog=PrologString(model);res=get_evaluatable().create_from(prog).evaluate()'),
  pyget(res,Result).



  