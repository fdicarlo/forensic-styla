% This is a demonstration of module mechanics as implemented by use/1, module using another module, directly accessing its internal predicates.

module(prefixed).

:- use(loops).

rule(X) :- 
   loops__test(X),
   if (
       X == 'hello', 
       (
           Y is 2+2,
           writel(['Y = ',Y])
       ),
       writel([X,'\n']) 
   ).