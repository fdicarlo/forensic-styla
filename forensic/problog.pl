:-  use(problog).

:-  problog('forensic/fsm-prob.pl',R),   % Process ProbLog model
    member(M-P,R),                       % Iterate through possible solutions X and their probabilities P
    P =\= 0.0,                           % For each solution that has a non-zero probability
    writel([M,'  -   ',P,'\n']).         % print it out