% current_predicate(FN),FN = F / N, clause(F,C), varname(C,C1), file_writer(stdio,S),pretty(S,':-'(F,C1),'','   ')

plisting :- 
   file_writer(stdio,F),
   plisting(F).

plisting(F) :-
   novar_database(D),
   member(C,D),
   pretty_clause(F,C,'','    '),
   write(F,'\n\n').

pretty_clause(F,[H|[]],O,D) :-
   !,
   pretty(F,H,O,D),
   write(F,'.').

pretty_clause(F,[Head|Body],O,D) :-
   not(R == []),
   atom_concat(O,D,O1),
   pretty(F,Head,O,D),
   writel(F,[' :- ']),
   Body=[First|Rest],
   if(First == 'true', 
     pretty_clause_helper(F,Rest,O1,D),
     pretty_clause_helper(F,Body,O1,D)).

pretty_clause_helper(F,[],O,D).

pretty_clause_helper(F,[H|T],O,D) :-
   writel(F,['\n',O]),
   pretty(F,H,O,D),
   if(T == [],write(F,'.'),write(F,',')), 
   pretty_clause_helper(F,T,O,D).

pretty(F,T,O,_) :- 
   atomic(T),
   if(number(T),write(F,T),(to_quoted(T,QT),write(F,QT))).

pretty(F,T,O,_) :- 
   date(T),
   writel(['\',T,'\']).   

pretty(F,T,O,_) :- 
   functor(T,'.',2),
   arg(1,T,A),
   arg(2,T,B),
   to_quoted(A,QA),
   writel(F,['[',QA]),
   pretty_list(F,B).

pretty_list(F,A) :-
   atomic(A),
   if(A == [],true,
   (
      to_quoted(A,QA),
      writel(F,['|',QA])
   )),
   write(F,']').

pretty_list(F,[A|B]) :-
   to_quoted(A,QA),
   writel(F,[',',QA]),
   pretty_list(F,B).

pretty(F,T,O,D) :- 
   functor(T,',',2),
   arg(1,T,A),
   arg(2,T,B),
   atom_concat(O,D,O1),
   writel(F,['(\n',O1]),
   pretty(F,A,O1,D),
   writel(F,[',\n',O1]),
   pretty_comma(F,B,O,O1,D),
   writel(F,['\n',O,')']).

pretty_comma(F,T,O,O1,D) :- 
   (
       functor(T,',',2),
       arg(1,T,A),
       arg(2,T,B),
       pretty(F,A,O1,D),
       writel(F,[',\n',O1]),
       pretty_comma(F,B,O,O1,D),
       !
   ;
       pretty(F,T,O1,D)
   ).

pretty(F,T,O,D) :- 
   functor(T,';',2),
   arg(1,T,A),
   arg(2,T,B),
   pretty(F,A,O,D),
   writel(F,['\n',O,';\n',O]),
   pretty(F,B,O,D).

pretty(F,T,O,D) :- 
   functor(T,Name,Narg),
   not(member(Name,[',','.',';',':-'])),
   Narg > 0,
   Narg1 is Narg+1,
   term_list(1,Narg1,T,Args),
   pretty(F,Name,O,D),
   write(F,'('),
   Args = [H|R],
   pretty(F,H,O,D),
   pretty_args(F,R,O,D),
   write(F,')').

pretty_args(_,[],_,_).

pretty_args(F,[H|T],O,D) :-
   write(F,','),
   pretty(F,H,O,D),
   pretty_args(F,T,O,D).
   

   