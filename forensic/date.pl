% Some examples of using date and time information

% Displaying path and last modified time of all files modified in the 21st century.
files_21a :-
   findall([P,M],
      (
          file(F),
          mtime_of(M,F),
          M >= \2001-01-01T00:00:00Z\,
          path_of(P,F)
      ),L),
    table('Files last modified in 21st Century',['Path','M-Time'],L).
 
% Same as above, but displaying only the Path
files_21b :-
   findall([P],
      (
          file(F),
          mtime_of(M,F),
          M >= \2001-01-01T00:00:00Z\,
          path_of(P,F)
      ),L),
    table('Files last modified in 21st Century',['Path'],L).

% Same as above, but using mtime() function in expression rather than mtimeof/2 predicate
files_21c :-
   findall([P],
      (
          file(F),
          mtime(F) >= \2001-01-01T00:00:00Z\,
          path_of(P,F)
      ),L),
    table('Files last modified in 21st Century',['Path'],L).
 