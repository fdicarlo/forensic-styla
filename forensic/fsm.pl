% Euclid's algorithm for calculating greatest common divisor expressed as a Finite State Machine

% Single-step transition function
f([A0,B0,IP0,R0],[A1,B1,IP1,R1]) :- IP0==1, A0 \== 0,A1 is A0, B1 is B0, IP1 is 2, R1 is R0.
f([A0,B0,IP0,R0],[A1,B1,IP1,R1]) :- IP0==1, A0 == 0,A1 is A0, B1 is B0, IP1 is 5, R1 is R0.
f([A0,B0,IP0,R0],[A1,B1,IP1,R1]) :- IP0==2, B0 \== 0,A1 is A0, B1 is B0, IP1 is 3, R1 is R0.
f([A0,B0,IP0,R0],[A1,B1,IP1,R1]) :- IP0==2, B0 == 0,A1 is A0, B1 is B0, IP1 is 4, R1 is R0.
f([A0,B0,IP0,R0],[A1,B1,IP1,R1]) :- IP0==3, A0 > B0,A1 is A0, B1 is B0, IP1 is 7, R1 is R0.
f([A0,B0,IP0,R0],[A1,B1,IP1,R1]) :- IP0==3, A0 =< B0,A1 is A0, B1 is B0, IP1 is 6, R1 is R0.
f([A0,B0,IP0,_],[A1,B1,IP1,R1])  :- IP0==4, A1 is A0, B1 is B0, IP1 is 8, R1 is A0.
f([A0,B0,IP0,_],[A1,B1,IP1,R1])  :- IP0==5, A1 is A0, B1 is B0, IP1 is 8, R1 is B0.
f([A0,B0,IP0,R0],[A1,B1,IP1,R1]) :- IP0==6, A1 is A0, B1 is B0-A0, IP1 is 2, R1 is R0.
f([A0,B0,IP0,R0],[A1,B1,IP1,R1]) :- IP0==7, A1 is A0-B0, B1 is B0, IP1 is 2, R1 is R0.
f([A0,B0,IP0,R0],[A1,B1,IP1,R1]) :- IP0==8, A1 is A0, B1 is B0, IP1 is 8, R1 is R0.

% Calculating state of the machine after T steps. 
model(ST,T) :- T >0, Tprev is T-1, model(STprev,Tprev), f(STprev,ST).

% Note that the initial state (at time T=0) is *NOT* specified in this file.
% The initial state needs to be added to the predicate database as a separate clause of the form 
%
% model([A,B,1,0],0).
%
% where A and B are the input values to the algorithm. Apart from specifying concrete values 
% for A and B,a range of possible values can be defined as a probability distribution 
% using ProbLog syntax as shown in the file fsm-prob.pl
