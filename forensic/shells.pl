% A very simple emulation of python shell (does not support multiline input)

p :- 
   silent,                        % supress normal prolog output
   repeat,                        % start eternal loop
      readline('>>> ',Line),      % read a line from console
      if(Line == '',              % if the line is empty,
      (
          !, nosilent, fail       % restore normal prolog output and terminate
      ),             
      (
          python(Line,false,Exc), % otherwise, feed the line to the python interpreter (in single-line mode)
          write(Exc),nl           % print returned error message (if any)
      )).


% A very simple emulation of Unix shell (does not handle cd or io redirection)

u :- 
   silent,
   repeat,          
      readline('$ ',Line), 
      if(Line == '',
         (!, nosilent, fail),
         (exec(Line,Output),write(Output))).