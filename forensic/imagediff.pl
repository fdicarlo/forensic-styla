% This example shows how DFP can be used to compare two disk images to determine differences

% image_name(?Image,?Name)
image_name(I,N) :- 
    image(I),
    tskname(I,N).

% image_files(+Image,-ListOfFiles)
image_files(I,Fs) :- 
    tskfind(I,'%','%',Fs).

% file_path(+File,-[Path-File])
file_path(F, '-'(Pathname,F)) :- 
    tskpath(F,_P),
    match_groups(_P,'/[^/]*/(.*)',[_,Pathname]).

% file_attrib(+File,-[Attrib-File])
file_attrib(F,'-'([Pathname, CrTime, MTime, ATime, CTime],F)) :-
    tskpath(F,_P),
    match_groups(_P,'/[^/]*/(.*)',[_,Pathname]),
    tskcrtime(F,CrTime),
    tskctime(F,CTime),
    tskatime(F,ATime),
    tskmtime(F,MTime).

changes(ImageNameX,ImageNameY,NewInY,MissingInY,ChangedInY) :- 
 
    image_name(X,ImageNameX),
    image_files(X,XFiles),
    maplist(file_path,XFiles,XPaths),

    image_name(Y,ImageNameY),
    image_files(Y,YFiles),
    maplist(file_path,YFiles,YPaths),

    keysubtract(YPaths,XPaths,NewInY),
    keysubtract(XPaths,YPaths,MissingInY),
  
    keysubtract(YPaths,NewInY,OldInY),
    pairs_values(OldInY,OldInYFiles),
    maplist(file_attrib,OldInYFiles,OldInYAttrs),
    maplist(file_attrib,XFiles,XAttrs),

    keysubtract(OldInYAttrs,XAttrs,ChangedInY).
    

