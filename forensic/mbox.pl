mbox([]) ::= end_of_data, {!}.         % Eudora mailbox could be empty
mbox([M|T]) ::= message(M), mbox(T).   % or it can contain one or more messages

% Each mail message constsits of a header followed by message data
message(M) ::= msgHeader, msgData(M).  

% The header is a text line 'From ???@??? ....\n'
msgHeader ::= `[\r\n]*From \?\?\?@\?\?\?[^\n]+\n`. 

% The message data is everything up-to (but not including) either the next message header 
% or the end of data
msgData(M) ::= `(.*?)(?:From \?\?\?@\?\?\?[^\n]+\n)`([_,M]), {!}. 
msgData(M) ::= `.*?`([M]), end_of_data.


% To parse Euroda message box, we need to load it into some variable X
% and then parse it using the above specification:
% 
% parse(mbox(MBX),X)
%
% it will parse X according to mbox specification and generate list 
% of email messages contained in the mailbox, which is unified with 
% the variable MBX

% To check if the given file is a Eudora mailbox, we can just check 
% if it begins with a messageHeader. 

isEudoraMbox(X) :- parse(msgHeader,X).

% The following predicate attempts to parse given case object as a Eudora mailbox. 
% If successful, the parsing generates a list of individual mail messages, 
% which are written into individual files in the case folder and
% added to the case as derived children of the mailbox object.

procEudoraMbox(X) :- 
    parse(mbox(MBX),X),
    name_of(MboxName,X),
    atom_concat('ModuleOutput/EudoraProc/',MboxName,OutputFolder),
    mkcasedir(OutputFolder),
    member(Msg,MBX),
    procMsg(Msg).

% We create individual sub-folder for each message in the mailbox
% If the given message is not a MIME-encoded, then its plain text is the only thing that is stored there
% If the given message *is* MIME encoded, then its attachments are extracted, decoded and stored into 
% the folder alongside with the message text.

mailMsg ::= mimeMsg, {!}.
mailMsg ::= plainMsg. 

mimeMsg ::=


      mkuniquefile(MsgFile,OutputFolder),
      open(MsgFile,w,F), 
      write(F,Msg), 
      stop(F),
      addderivedfile(MsgFile,X).
      


