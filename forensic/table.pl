% Display paths and sizes of all files in a GUI table

tabledemo :-
   findall(
       [P,S,M],                    % Variables (each variable generates data for a separate column)
       (                             
           file(X),
           path_of(P,X),
           size_of(S,X),
           mtime_of(M,X)
       ),
       Data),                      % Every combination of variable values matching the above query is added to the Data list
                                   % Data = [ [/test.txt,256,'1970-01-01T00:00:00Z'], [/hive.dat,1156,'1999-11-20T15:07:49Z'], ... ]

   table(
       'All Files',                % Title of the table window
       ['Path', 'Size', 'MTime'],  % Columns headers (the number and order of headers must match the first parameter)
       Data).                      % Data to display

 