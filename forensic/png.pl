% PNG File Parser 
% ---------------
% An illustration of how to use binary parsing in DFIRE Forensic Prolog.
%
% Binary grammars of DFIRE Forensic Prolog are broadly similar to 
% Definite Clause Grammar statements, but differ from DCG in specific detail:
%
% 1) The '::=' operator of BNF metalanguage is used for defining non-terminals.
%
% 2) A number of pre-defined non-terminals exist for recognising common basic data 
% structures. For example, u32be(X) is a pre-defined non-terminal that 
% consumes four bytes from the input data and decodes them as an unsigned 32-bit integer 
% stored in big-endian order. The result of the decoding is unified with the variable X.
%
% 3) Terminal symbols are defined using back-quoted strings like `MZ`. Terminal strings may
% contain backslash escape sequences and regular expressions following Java regex syntax. 
% For example, a terminal string `[01]{4}` matches a group of four binary digits starting 
% at the current position of the parses in the input data. 
%
% Special parametrized syntax is used for extracting matches of regex groups. For example,
% the following terminal string `[0-9a-fA-F]{4}-([0-9a-fA-F])`(M) matches a group that consits 
% of two groups of four hexadecimal digits separated by a dash. Suppose that the input data is 
% 
% FF04-893A
%
% Terminal string `[0-9a-fA-F]{4}-([0-9a-fA-F])`(M) will match this data, the output variable 
% M will be bound to the list ['FF04-893A','893A']. The first element of the produced list 
% corresponds to the entire matched string, the second element correspind to the first matched group,
% and so on.
%
% Binary grammar clauses are processed using parse(Grammar,Data) predicate.
%
% 4) Similar ot DCG syntax, curly braces { } are used for inlining normal prolog 
% code into the parsing process to post-process parsing results. 
%
% The following example specifies png(Info) grammar that describes the high-level structure of 
% a PNG file. 'Info' is the output variable that contains the list of image properties 
% extracted from the PNG data by the parsing process.

% A PNG file consists of a signature block followed by a sequence of chunks
png(Info) ::= 
   pngSignature,
   pngChunks(Info).

% PNG signature consists of 8 bytes:
pngSignature ::= `\x89PNG\r\n\x1A\n`.

% The sequence of chunks ends when we reach the end of data.
pngChunks(Info) ::= 
   end_of_data, 
   {Info=[]}. 

% The sequence of chunks may consist of a header chunk followed by more chunks.
% We extract image parameters from the header and preprend them as key-value pairs 
% to the list of information we are constructing.
pngChunks(Info) ::= 
   pngHeaderChunk(H,W,B,C,M,F,I),
   pngChunks(MoreInfo),
   {Info = [image_height-H, image_width-W, image_bit_depth-B, colorType-C, image_compression_Method-M, image_filter-F, image_interlace-I | MoreInfo]}.

% 1. The header chunk has fixed length 13, Type field containing 'IHDR', and a list of image parameters (width, height, etc.)
pngHeaderChunk(Height,Width,BitDepth,Color,CompressionMethod,Filter,Interlace) ::=
  u32be(13),
  `IHDR`,
  u32be(Width),
  u32be(Height),
  u8(BitDepth),
  pngColorType(Color),
  u8(CompressionMethod),
  u8(Filter),
  u8(Interlace),
  u32be(CRC).

% An example of custom parser for 8-bit color encoding scheme. This predicate consumes a single byte of data ( u8(TypeCode) ), analyzes it,
% and binds the output variable 'Type' to the corresponding textual description of the color encoding scheme.
pngColorType(Type) ::= 
   u8(TypeCode),
   { 
     ( 
       member( TypeCode-Type, [0-'Grayscale', 2-'Truecolour', 3-'Indexed-colour', 4-'Greyscale with alpha', 6-'Truecolour with alpha' ] ), ! 
     ;
       Type='INVALID'
     )
   }.

% 2. Another type of PNG chunk is the 'tEXt' chunk that contains a single piece of the image-related metadata as a textual key-value pair.  
% We parse the metadata and pre-pend it (as a key-value pair) to the list of information we are constructing.
% Observe that tEXt chunk can be (and usually is) followed by more PNG chunks. 
pngChunks(Info) ::= 
   pngtEXtChunk(K,V),
   pngChunks(MoreInfo),
   {Info = [metadata-[K-V] | MoreInfo]}.

% A tEXt chunk has the header type 'tEXt' followed by a variable-length data.
pngtEXtChunk(Key,Value) ::= 
  u32be(Length),
  `tEXt`,
  bytes(Length,Data),
  u32be(CRC),
  {
    parse(pngKeyValue(Key,Value),Data)
  }.

% The content of a tEXt chunk data is a key-value pair. The key is separated from the value by NUL character '\0'
% DFIRE Forensics Prolog allows regular expressions in terminal symbols. It uses paramerized terminals of the form `someregex`(GroupMatches)
% to capture the content of matching regex groups:

pngKeyValue(Key,Value) ::= `([^\x00]+)\x00(.*)`([_,Key,Value]).

% The sequence of chunks may contain chunk types that we do not know or do not want to
% parse. We define special non-terminal symbol pngGenericChunk to skip over them.
pngChunks(Info) ::= 
  pngGenericChunk,
  pngChunks(Info).

% General format of PNG chunk consists of a 32-bit Length field and a four-byte Type field followed 
% by a sequence of bytes of the specified length and a four-byte CRC checksum:
pngGenericChunk ::= 
  u32be(Length),
  bytes(4,Type),
  bytes(Length,_),
  u32be(CRC).


% The follwing query uses the above defined grammar to obtain Width and Height information 
% about every PNG files in the case.
%
% It iterates over the files that have extension '.png', and tries to parse every such file 
% according to png(Info) grammar. If parsing is successful, it reports the value of 'image_width' and 'image_height' 
% key-value pairs from the Info list returned by the parsing process.
/* 

file(_X,'%','%.png'),content_of(_C,_X),once(parse(png(_I),_C)),member(image_width-W,_I),member(image_height-H,_I)

*/
