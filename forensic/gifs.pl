
gif(X) :- file(X),
          content_of(C,X,0,3),
          matches(C,'^GIF').

print_all_gifs :- gif(X),
                  path_of(P,X),
                  println(P).

print_susp_gifs :- gif(X),
                   path_of(P,X),
                   not(matches(P,'.*.gif$',i)),
                   println(P).
