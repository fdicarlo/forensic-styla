% This file demonstrates how to define new compiled predicates written in scala (built-ins) 
% using tskscalafun/3. The invocation syntax is as follows:
%
% tskscalafun(+fname,+narg,+scalaCode).
%
% The effect of the invocation is:
%
% 1) scalaCode is dynamically compiled, and the built-in function fname_builtin/narg is added to the table of builtin functions in the TermReader
% 2) the predicate fname/narg is added to the prolog database
%
%     fname(_1,_2,_3,...) :- fname_builtin(_1,_2,_3,...).
%
%  The second step is needed, because the parsing of the file occurs *before* dynamic compilation, and a normal (i.e. non-builtin) predicate is
%  needed to permit predicates defined in the same file to use the dynamically generated builtin.
%

:- tskscalafun('hello',2,'              // Define built-in predicate with a 2 arguments hello(Name,Output)  

val name = getArg(0);                   // Get first argument

val result =  new Fun("hi",Array[Term](name));   // Create term hi(Name)

putArg(1,result,p);                     // unify the result with second argument of the poredicate
1                                       // return 1 indicating that predicate succeeded (0 = failed)

').

scalaHello :- 
    hello('John',X),
    writel(['\n\nhello(John,X) returned: ',X,'\n\n']).

:- scalaHello.
