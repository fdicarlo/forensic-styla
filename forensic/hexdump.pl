
hexdump(A,[]) ::= end_of_data,{ ! }.
hexdump(A,[L|D]) ::= dumpline(A,L),{Anext is A + 16},hexdump(Anext,D).
dumpline(A,[HexA,B0,B1,B2,B3,B4,B5,B6,B7,B8,B9,B10,B11,B12,B13,B14,B15,Ascii])::=
   capture((
      hexbyte(B0),hexbyte(B1),hexbyte(B2),hexbyte(B3),hexbyte(B4),hexbyte(B5),hexbyte(B6),hexbyte(B7),
      hexbyte(B8),hexbyte(B9),hexbyte(B10),hexbyte(B11),hexbyte(B12),hexbyte(B13),hexbyte(B14),hexbyte(B15)
   ),Line),
   {to_hex(A,8,HexA),to_dotted_ascii(Line,Ascii)}.
hexbyte(B) ::= end_of_data,{B = '  '}.
hexbyte(B) ::= u8(ByteVal),{to_hex(ByteVal,2,B)}.

showdump(D) :- parse(hexdump(0,H),D),
               table('HexDump',['Offset','0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F','ASCII'],H).

printdump(D) :- parse(hexdump(0,H),D),
                member([Off,B0,B1,B2,B3,B4,B5,B6,B7,B8,B9,B10,B11,B12,B13,B14,B15,A],H),
                writel([Off,' | ',B0,' ',B1,' ',B2,' ',B3,' ',B4,' ',B5,' ',B6,' ',B7,' ',B8,' ',B9,' ',B10,' ',B11,' ',B12,' ',B13,' ',B14,' ',B15,' | ',A,'\n']).