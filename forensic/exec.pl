% Sample of running external command 'ls -l', getting its input and parting it using regexps

ls(Filename,Date,Size) :- 

      % 1. Run external command with its STDOUT being collected into temporary variable _O and no data ('') going into its STDIN

      exec('ls -l',_O,''),  

      % 2. Use match_groups() to split the produced listing into lines using regex grouping.
      %    Note that in [_,_L], _L matches regex group 1. The very first element of the list matches the entire regex.

      match_groups(_O,'([^\n]*)\n',[_,_L]),

      % 3. Use match_groups() again to extract file size, modification date, and file name from individual lines of text.
      %    Note that the size is extracted as a string (in the temporary variable _S) and needs to be converted to number.

      match_groups(_L,'[^0-9]+[0-9]+[^0-9]+([0-9]+)[ ]+([^ ]+[ ]+[^ ]+[ ]+[^ ]+)[ ]+([^ ]+)',[_,_S,Date,Filename]),
      
      % 4. Convert the size of file from string to number

      to_number(_S,Size).