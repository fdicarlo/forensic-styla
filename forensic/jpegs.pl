
jpeg(X) :- file(X),
          content_of(C,X,0,1024),
          matches(C,'JFIF').

print_all_jpegs :- jpeg(X),
                  path_of(P,X),
                  println(P).

print_susp_jpegs :- jpeg(X),
                   path_of(P,X),
                   not(matches(P,'.*.jpg$',i)),
                   println(P).

