% This file shows how ProbLog can be used to perform probabilistic reasoning about
% an algorithm defined as a finite state machine. Note that the algorithm itself 
% is defined in a separate file 'fsm.pl' and it uses standard prolog syntax.

% First we include that file into our model:

:- consult('fsm').

% The input values given to the algorithm determine the initial state of the  
% corresponding state machine. The initial state is not defined in 'fsm.pl' so
% we define it here as a probability distribution over possible value of input
% parameters A and B.

% Initial probability distribution.
% Suppose that A could have been any number from 1 to 10 with equal probability.

0.1::a(1);
0.1::a(2);
0.1::a(3);
0.1::a(4);
0.1::a(5);
0.1::a(6);
0.1::a(7);
0.1::a(8);
0.1::a(9);
0.1::a(10) <- true.

% Suppose that B also could have been any number from 1 to 10 with equal probability.

0.1::b(1);
0.1::b(2);
0.1::b(3);
0.1::b(4);
0.1::b(5);
0.1::b(6);
0.1::b(7);
0.1::b(8);
0.1::b(9);
0.1::b(10) <- true.

% The initial state of the algithm is model([A,B,1,0],0), where A and B belong to 
% probability distributions a() and b() respectively:

model([A,B,1,0],0) :- a(A),b(B).

% Now suppose that after 50 steps the algorithm produced gcd(A,B) value equal to 5,
evidence(model([_,_,8,5],50),true).

% Determine the likelihood of specific initial state of the machine given that evidence
query(model(ST,0)).

% The above model can be called from DFIRE Forensic Prolog using problog(File,Result)
% predicate and all non-zero problog probabilities can be shown using show_result(Result)
% predicate:
%
% ?- problog('forensic/fsm-prob.pl',R),show_result(R)