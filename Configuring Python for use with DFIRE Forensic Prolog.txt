Configuring Python for use with DFIRE Forensic Prolog

DFIRE Forensic Prolog uses JEP library to interact with Python interpreter. It needs to be installed on your machine as a Python package. Here is how you can do it on
Windows:

1. Download and Install JDK 8 from http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html  choose 64-bit (or 32-bit) version depending on your Windows installation.

2. Set environment variable JAVA_HOME = <path-to-installed-JDK-folder> 

3. Download and install latest Python 2.7 from http://www.python.org/  
choose 64-bit (or 32-bit) installer depending on your Windows.  Note that both Python and JDK should be the same in that regard (i.e. both 64-bit or both 32-bits).

4. Add Python installation folder (typically C:\Python2.7 ) to the System-wide environment variable Path

5. Download and install Microsoft C++ compiler v9.0 for Python from https://www.microsoft.com/en-us/download/details.aspx?id=44266 

6. Start Windows command line and install Python JEP package using Python�s PIP package manager:

python -m pip install jep 

Installation may request installation of .NET framework packages, in which case it does not seem complete successfully for the first time. Proceed with the installation of .NET packages  as requested, then re-run the above command (python �m pip install jep).

7. Copy produced jep.dll file from C:\Python27\Lib\site-packages\jep folder to either lib\x86 (for 32-bit OS) or lib\x86_64 (for 64-bit OS) folder in the DFIRE Forensic Prolog folder.

8. Copy produced jep-x.x.x.jar file from C:\Python27\Lib\site-packages\jep folder to lib\jar folder in DFIRE Forensic Prolog folder and rename it as jep.jar.

9. (Re-)compile DFIRE Forensic Prolog

10. Run DFIRE Forensic Prolog. The following command should work now:

?- python
>>> 2+2
4
>>>

If the above command works, you should be able to load and run python examples from forensic\ sub-folder in DFIRE Forensic Prolog folder.

11. To use probabilistic prolog package (problog) from KU Leuvian, intall numpy and problog modules in python:

python -m pip install problog
python -m pip install numpy

An example of using problog is in forensic\problog.pl file.