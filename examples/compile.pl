% Initial compiler (Prolog => Scala).

% compile(-Predicate/Arity, +ScalaFun)
compile(Predicate,ScalaFun,SupportingProlog) :- 
	predicate_clauses(Predicate,ClauseList),
	clauses_to_scala(ClauseList,ScalaFun,SupportingProlog).

% === Case 1: A predicate consisting of a single clause that consists *only* of builtins, 
% can be represented as a builtin function. 
clauses_to_scala(ClauseList,ScalaFun,SupportingProlog) :-
    Clauses = [Clause],  % If ClauseList contains only one clause
	Clause = (Head :- Body),
	contains_only_builtins(Body), % and the body contains only builtins
	!,
	clause_to_builtin(Head,Body,ScalaFun,SupportingProlog).

contains_only_builtins(','(X,R)) :- 
    is_builtin(X),
	contains_only_builtins(R),
	!.
contains_only_builtins(X) :- is_builtin(X).

clause_to_builtin(Head,Body,ScalaFun,SupportingProlog) :-
	Head =.. [Name | Args],
	length(Args,Arity),
	if (Arity =:= 0, BuiltinArity = 1, BuiltinArity = Arity),
	writes(BuiltinName,[Name,'_builtin']),
	builtin_initialisation(Args,Body,BuiltinName,Defs,Consts,TermMap),
	builtin_exec(Args,Body,TermMap,Exec),
	S = 
[
'package prolog.builtins
import prolog.terms._
import prolog.interp.Prog

final class ',Name,'_builtin() extends FunBuiltin("',Name,'", ',BuiltinArity,') {\n',
Defs,
'override def exec(p: Prog) = {\n',
Exec,
'}
}

object ',Name,'_builtin {\n',
Consts,
'}'],
	writes(ScalaFun,S),
	HeadTerm =.. [Name | Args],
	if (Arity =:= 0, BuiltinArgs = [ _ ], BuiltinArgs = Args),
	BuiltinTerm =.. [BuiltinName | BuiltinArgs],
	SupportingProlog = ':-'(HeadTerm,BuiltinTerm).

% This predicate creates scala code to create all subterms mentioned in the predicate.
% The produced output consists of three parts:
%
%   Defs: definitions of member variables for the instance of generated Builtin class,
%   Consts: definitons of constants as object variables (in scala, object is a singular
%           automatically created data structure that holds static variables and constants,
%           in our case, one object is created for each Builtin to hold constants)
%   TermMap: list of key-value pairs that links Prolog term to the
%            scala code that is used to access it. 
%

builtin_initialisation(Args,Body,BuiltinName,Defs,Consts,TermMap) :-
    commalist_to_list(Body,TermList),
	get_subterms(TermList,SubtermList1),
	remove_duplicate_terms(SubtermList1,SubtermList2),
	reverse(SubtermList2,SubtermList3),
	variable_terms(SubtermList3,VariableList),
	constant_terms(SubtermList3,ConstantList),
	Defs = VariableList,
	Consts = ConstantList.
	%terms_to_scala(ConstantList,BuiltinName,[],Consts,TermMap0),
	%terms_to_scala(VariableList,'this',TermMap0,Defs,TermMap).

terms_to_scala(TermList,Prefix,TermMap0,ScalaDefs,TermMap1).

get_subterms(TermList,SubtermList) :-
	foldl(append_subterms,[],TermList,SubtermList).
% auxiliary function use for concatenating subterm lists for
% constructed from individual terms in the clause body.
append_subterms(L0,X,L) :- subterms(X,S),append(L0,S,L).

builtin_exec(Args,Body,TermMap,Exec) :-
	Exec = ''.

% === Case 2: We compile predicate as a specialised term source, but it can be done only
% if all predicates it calls haev already been compiled into corresponding term sources.
clauses_in_scala(ClauseList,ScalaFun) :-
    contains_only_compiled_pred(ClauseList),
	clauses_as_termsource(ClauseList,ScalaFun).

contains_only_compiled_pred(ClauseList) :- fail.
clauses_as_termsource(ClauseList,ScalaFun) :- fail.

% === Auxiliary predicates ==

predicate_clauses(Name/Arity,ClauseList) :-
    % First we convert fun/N notation to fun(_,_,...,_) term 
	fun(Name,Arity,Head),   
    % and search for clauses whose head matches it.
    findall(Clause,
	(
		clause(Head,Body),
		% At the same time we do some source level transformations.
		simplify_clause_head(Head,Body,Head1,Body1),
	    Clause = (Head1 :- Body1)
	),
	ClauseList).

% Transforming clause by moving any restrictions from the head of the clause to the
% beginning of its body. For example, a clause
%
% rule(Z,fun(X,Y)) :- body
%
% is transformed into 
%
% rule(Z,N) :- N=fun(X,Y),body.
simplify_clause_head(Head,Body,NewHead,NewBody) :-
    Head =.. [Name | Args],
	unburden_arguments(Args,NewArgs,Constr),
	NewHead =.. [Name | NewArgs],
	prepend_terms(Constr,Body,NewBody).

% unburden_arguments(+Args,-NewArgs,-Constr)
% Iterates through the given list of arguments, and copies them one by one into NewArgs.
% Any argument in Args that is not a variable is replaced by a fresh varible (NewVar) and 
% a condition of the form NewVar=OldArg is added to the Constr list. 
unburden_arguments([],[],[]).
unburden_arguments([H|T],[H|NT],C) :- var(H),unburden_arguments(T,NT,C).  
unburden_arguments([H|T],[NewVar|NT],[NewVar=H|C]) :- nonvar(H),unburden_arguments(T,NT,C).

% Prepends given list of terms to the comma-separated list of terms in the
% clause body.
prepend_terms([],X,X).
prepend_terms([H|T],X,Y) :- Y = ','(H,Z), prepend_terms(T,X,Z).

% An equivalent of member() for comma-separated list of terms.
commalist_element(','(Element,RestOfList),Element).
commalist_element(','(Element,RestOfList),X) :- commalist_element(RestOfList,X).
commalist_element(Element,Element) :- not(Element = ','(_,_)).	

% Convert comma-separated list of terms (Body of a clause) into normal prolog list
commalist_to_list(','(Element,Rest),[Element|ListOfRest]) :- 
	commalist_to_list(Rest,ListOfRest).
commalist_to_list(Element,[Element]) :- 
	not(Element = ','(_,_)).

% Make list of all subterms of the given Term, put it in the order of 
% construction (from atoms to compound terms), then remove duplicates.
subterms(Term,Subterms):-
    subterms_aux(Term,[],Subterms0),
	reverse(Subterms0,Subterms1),
    remove_duplicate_terms(Subterms1,Subterms2),
	reverse(Subterms2,Subterms).

% The following auxiliary predicate visit all subterms of the given Term 
% in the breadth-first order. All sub-terms are appended to TermList 
% in the order of visitation.
subterms_aux(Term,TermList,NewTermList) :- 
	not(compound(Term)),
	!,
	NewTermList = [Term|TermList].
subterms_aux(Term,TermList,NewTermList) :-
	Term =.. FlatList,
	drop(1,FlatList,Args),
	foldr(subterms_aux,TermList,Args,NewTermList0),
	NewTermList = [Term|NewTermList0].

% Remove duplicate entities from the list. Since the list of subterms may
% contain variable, a special version of member() predicate is defined, 
% which does not unify such variables.
remove_duplicate_terms(TermList1,TermList2) :- 
	foldl(append_if_not_member,[],TermList1,TermList2).

append_if_not_member(TermList,X,TermList) :- member_no_unification(X,TermList),!.
append_if_not_member(TermList,X,[X|TermList]).

member_no_unification(X,[Y|_]) :- X == Y.
member_no_unification(X,[_|Y]) :- member_no_unification(X,Y).

% Exclude non-ground terms from given list of terms.
constant_terms(TermList,ConstTermList) :-
	foldl(append_if_ground_term,[],TermList,ConstTermList0),
	reverse(ConstTermList0,ConstTermList).

append_if_ground_term(TermList,X,[X|TermList]) :- ground(X),!.
append_if_ground_term(TermList,X,TermList).

% Exclude ground terms from given list of terms.
variable_terms(TermList,ConstTermList) :-
	foldl(append_if_not_ground_term,[],TermList,ConstTermList0),
	reverse(ConstTermList0,ConstTermList).

append_if_not_ground_term(TermList,X,[X|TermList]) :- not(ground(X)),!.
append_if_not_ground_term(TermList,X,TermList).
