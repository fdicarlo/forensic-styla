package prolog.forensic

import prolog.io
import prolog.terms._

import scala.collection.JavaConverters._
import scala.collection.mutable._
import scala.util.matching.Regex
import scala.io._
import BigInt._
import java.util.UUID
import java.io._
import org.sqlite._
import org.sleuthkit.datamodel._

class TSKContent(content: Content) extends SystemObject {
   val c = content;
   
   def getId : BigDecimal = TSKContent.zero

   override def toString(): String = {
      return "{" + c.getUniquePath() + "}" 
  }

  override def unify(that: Term, trail: Trail): Boolean =
  {
    if (that.isInstanceOf[TSKContent] && (that.asInstanceOf[TSKContent].c.getUniquePath() == c.getUniquePath()))
      true
    else
      false
  }

  def getSize = c.getSize

  def read(buf: Array[Byte], offset: Long, length: Long) = c.read(buf,offset,length)

}

object TSKContent {
   val zero = BigDecimal(0)
}
