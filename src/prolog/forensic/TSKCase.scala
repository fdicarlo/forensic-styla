package prolog.forensic

import prolog.io._
import prolog.terms._

import scala.collection.JavaConverters._
import scala.collection.mutable._
import scala.util.matching.Regex
import scala.io._
import java.util.UUID
import java.io._
import org.sqlite._
import org.sleuthkit.datamodel._
import java.nio.file._
import java.nio.file.attribute._

/** System-wide TSK Case object */

object TSKCase {

  var fileName = ""
  var active:Boolean = false
  var tskcase:SleuthkitCase = null
  var tskImages:Seq[Image] = null
  var tskImagePaths:Map[java.lang.Long,java.util.List[String]] = null
  var images:ListBuffer[Term] = null

  var datasources:ListBuffer[Term] = null

  def openCase(fname: String) : Int = {
     close()
     val absname = new java.io.File(fname).getAbsolutePath
     tskcase =  SleuthkitCase.openCase(absname)
     updateCaseInfo()
     fileName = fname
     active = true
     println ("% case opened")
     1
  }

  def newCase(fname: String) : Int = {
     close()
     val absname = new java.io.File(fname).getAbsolutePath
     tskcase = SleuthkitCase.newCase(absname)
     updateCaseInfo()
     fileName = fname
     active = true
     println ("% new case created")
     1
  }

  def close() {
     if (active)
     {   
       print("\n% closing case. ")
       tskcase.close()
       fileName = ""
       tskImages = null
       tskImagePaths = null
       images = null
       datasources = null
       active = false
     }
  }

  def updateCaseInfo() {
    tskImages = tskcase.getImages().asScala
    tskImagePaths = tskcase.getImagePaths().asScala
    images = new ListBuffer[Term]()
    for ( i <- tskImages )
    {
       images += new TSKContent(i)  
    }
    datasources = new ListBuffer[Term]()
    for ( i <- tskcase.getRootObjects.asScala )
    {
      datasources += new TSKContent(i)  
    }
  }

  def getDbDirPath() = tskcase.getDbDirPath;

  def getFilesMatchingRegex(dir: String, regex: scala.util.matching.Regex) = {
      new java.io.File(dir).listFiles
             .filter(file => regex.findFirstIn(file.getName).isDefined)
             .map(file => file.getPath)
             .toArray
  }


  def addImage (fname : String) : Int = {
    if (active) 
    {
       if (new java.io.File(fname).exists)
       {
          val p = tskcase.makeAddImageProcess("", true, false)
          // p.run(UUID.randomUUID().toString(), Array(fname)) /* This is for Sleuthkit V4.3 and above*/
          p.run(Array(fname)) /* This is for Sleuthkit V4.2 */
          p.commit()
          updateCaseInfo()
          println ("% image successfully added")
          1  
       }
       else
       {
          println ("% image file(s) not found!")
         0
       }
    }
    else -1  /* The meaning of adding image is undefined if the case is not active */
  }

  def addImageCreatingList(fname : String) : Int = {
    if (active)
    { 
       var imgFile = new java.io.File(fname)
       if (imgFile.getParentFile == null)
       {
          imgFile = new java.io.File("."+java.io.File.separator+fname)
       }
       // Escape all characters that have special meaning in regular expressions: $ --> \$, ( --> \( etc.
       val prefName1 = imgFile.getName.replaceAll("([\\\\\\[\\]\\{\\}\\.\\(\\)\\|\\+\\$\\^])","\\\\$1")
       // Replace all trainilng digits in the filename extension with '?', e.g. PAVEL05.E01 becomes PAVEL05.E??
       var prefName2 = prefName1
       var prefName2n = prefName2.replaceAll("[0-9]([^0-9\\.]*)$","?$1")
       while (prefName2n != prefName2) {
          prefName2 = prefName2n
          prefName2n = prefName2.replaceAll("[0-9]([^0-9\\.]*)$","?$1")
       }
       // Replace all '*' wildcards in the file name with '.*'
       val prefName3 = prefName2n.replaceAll("\\*",".*")
       // Replace all '?' wildcards in the file name with '.'
       val prefName4 = prefName3.replaceAll("\\?",".")
       addImageRe(imgFile.getParentFile.getCanonicalPath, prefName4)
    }
    else -1  /* The meaning of adding image is undefined if the case is not active */
  }

  def addImageRe(dirname : String, fname : String) : Int = {
    if (active) 
    {
       val p = tskcase.makeAddImageProcess("", true, false)
       val files = getFilesMatchingRegex(dirname, new scala.util.matching.Regex(fname)) 
       if (files.length > 0)
       {
          // p.run(UUID.randomUUID().toString(), files.sorted) /* This is for Sleuthkit V4.3 and above*/
          p.run(files.sorted) /* This is for Sleuthkit V4.2 */
          p.commit()
          updateCaseInfo()
          println ("% image successfully added")
          1
       }
       else
       {
          println ("% image file(s) not found!")
          0
       }
    }
    else -1  /* The meaning of adding image is undefined if the case is not active */
  }

  def addLocal (fname : String, parent : TSKContent) : Content = {
    if (active) 
    {
       val path:Path = FileSystems.getDefault().getPath(fname);
       var localFile = path.toFile
       if (localFile.exists)
       {
          val attrs = Files.readAttributes(path, classOf[BasicFileAttributes]);
          val rootObjects = tskcase.getRootObjects().asScala;
          var localFiles: AbstractFile = null;
          if (parent != null)
          {
            localFiles = parent.c.asInstanceOf[AbstractFile];
          }
          else
          {
              for (a <- rootObjects) { a match { case c:VirtualDirectory => localFiles = c; case _ => null }}
              if (localFiles == null)
              {
                 localFiles=tskcase.addVirtualDirectory(0,"LocalFiles")
              }
          }
          val lf = tskcase.addLocalFile(localFile.getName, localFile.getCanonicalPath,
                       attrs.size, attrs.lastModifiedTime.toMillis/1000, attrs.creationTime.toMillis/1000, attrs.lastAccessTime.toMillis/1000, attrs.lastModifiedTime.toMillis/1000,
                       !(attrs.isDirectory), localFiles)
          if (lf == null)
          {
             // println ("% error adding local file")
             lf
          }
          else
          {
             updateCaseInfo()
             //println ("% local file successfully added")
          }  
          if (attrs.isDirectory)
          {
             // Recurse through children of that directory
             val stream: DirectoryStream[Path] = Files.newDirectoryStream(path)
             try 
             {
                for(child <- stream.asScala) 
                {
                   addLocal(child.toString,new TSKContent(lf));
                }
             } catch 
             {
               case _ =>
                // IOException can never be thrown by the iteration.
                // In this snippet, it can only be thrown by newDirectoryStream.
                IO.warnmes("Not all children of "+path+" have been added!");
             } 
             stream.close();
          }
          lf 
       }
       else
       {
         //println ("% local file not found!")
         null
       }
    }
    else null  /* The meaning of adding local file is undefined if the case is not active */
  }


  
  def addDerivedFile (fname : String, parent : TSKContent, derivation : String) : Content = {
    if (active) 
    {
       val path:Path = FileSystems.getDefault().getPath(fname);
       val relpath = FileSystems.getDefault().getPath(tskcase.getDbDirPath).relativize(path);
       var localFile = path.toFile
       if (localFile.exists)
       {
          val attrs = Files.readAttributes(path, classOf[BasicFileAttributes]);
          val lf = tskcase.addDerivedFile(
                          localFile.getName, 
                          relpath.toString,
                          attrs.size, 
                          attrs.lastModifiedTime.toMillis/1000, 
                          attrs.creationTime.toMillis/1000, 
                          attrs.lastAccessTime.toMillis/1000, 
                          attrs.lastModifiedTime.toMillis/1000,
                          !(attrs.isDirectory), 
                          parent.c.asInstanceOf[AbstractFile],
                          derivation,
                          "DFIRE Forensic Prolog",
                          "v0.1",
                          "")
          if (lf == 0)
          {
             println ("% error adding local file")
             lf
          }
          updateCaseInfo()
          println ("% local file successfully added")
          lf
       }
       else
       {
         println ("% local file not found!")
         null
       }
    }
    else null  /* The meaning of adding local file is undefined if the case is not active */
  }



  override def toString(): String = {
    if (active) { 
      return "{" + getClass() + ": database:" + fileName + "}" 
    }
    else
    {
      return "{" + getClass() + ":" + "closed state" + "}"
    }
  }

  def getMtimeById( id:Long) : BigDecimal =
  {
     BigDecimal(tskcase.getAbstractFileById(id).getMtime)
  }
}






