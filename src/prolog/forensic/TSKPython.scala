package prolog.forensic
import prolog.terms._
import jep._

class TSKPython() extends SystemObject {

  var jep:Jep = null

  def start { jep = new Jep(new JepConfig().setInteractive(true).setRedirectOutputStreams(true)) }

  def stop {
     if (jep != null) {
       jep.close();
     }
  }

  def setInteractive(b: Boolean) {
    if (jep != null) {
      jep.setInteractive(b);
    }
  } 

  def set(v: String, o: Object) {
    if (jep != null) {
      jep.set(v,o);
    }
  }

  def getValue(v: String) = {
    if (jep != null) {
      jep.getValue(v);
    }
    else null
  }

  def eval(s : String) : Boolean = {
    if (jep != null) {
      jep.eval(s);
    }
    else false;
  }

  def runScript(s : String) = {
    if (jep != null) {
      jep.runScript(s);
    }
  }

  override def toString() =
    "{ Jep Object : " + jep.toString() + " }"
}

object TSKPython extends TSKPython {
   start
}