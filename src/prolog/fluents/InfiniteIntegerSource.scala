package prolog.fluents
import prolog.terms._

class InfiniteIntegerSource(a: Int, var x: Int, b: Int) extends TermSource {

  def getElement() = {
      val R = SmallInt(x)
      x = a * x + b
      R
  }

  override def toString() =
    "Infinite counter{(x->" + a + "*x+" + b + ")=" + x + "}"
}