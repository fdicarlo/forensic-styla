package prolog.io
import prolog.terms._
import prolog.interp.Prog
import jline.console.ConsoleReader
import jline.console.history.FileHistory
import java.net.{ServerSocket, Socket, InetAddress}
import java.io.{File, PrintStream, InputStreamReader}

object IO {

  def time = System.currentTimeMillis()

  val t0 = time

  val configFile = "./dfpconfig.pl"

  var configuration: Term = null

  val default_configuration = new Fun("dfpconfig", 
             Array( 
               Cons.fromList(
                 List(
                  new Fun("-",
                    Array(
                      new Const("caseFile"),
                      Const.empty
                    )
                  )
                 )
               )
             ))
    

  def start = {
    println("DigitalFIRE Forensic Prolog (C) Pavel Gladyshev, 2016")
    println("based on STYLA Prolog System (C) Paul Tarau, 2011-2012")
    println("Press Ctrl-D to exit (on Windows press Ctrl-Z + Enter) \n")

    read_configuration()
  }

  def stop = {
    println("Prolog execution halted")
    //println("total time in ms=" + (time - t0))

    /* Writing system configration */
    write_configuration()

    /* Saving command line history */
    stdio.getHistory.asInstanceOf[FileHistory].flush()
    System.exit(0)
  }


  def read_configuration() {
    /* Attempt reading system configuration */
    try {
       configuration = new TermParser().file2clauses(configFile).head.head;
    } catch { 
      case _ => {
          IO.warnmes("Configuration file "+configFile+" not found. Falling back to default configuration.");
          configuration = default_configuration
      }
    }
  }

  def write_configuration() {
    try{
      val tw = new TermWriter(configFile)
      tw.putElement(new Const(TermParser.term2string(configuration)));
      tw.putElement(new Const("."))
    } catch {
      case _ => IO.warnmes("Cannot write into configuration file " + configFile + " !")
    }
  }

  final def find_file(f0: String): String = {
    if ("stdio" == f0) return f0
    val fs1 = List(f0 + ".pro", f0 + ".pl", f0)
    val fs2 = fs1.map(x => "forensic/" + x)
    val fs3 = fs1.map(x => "examples/" + x) 
    val fs4 = fs1.map(x => "progs/" + x)
    val fs = (fs1 ++ fs2 ++ fs3 ++ fs4).dropWhile(x => !new File(x).exists())
    if (fs.isEmpty) {
      IO.warnmes("file not found: " + f0)
      null
    } else
      fs.head
  }

  var debug_ss: ServerSocket = null
  var debug_s: Socket = null
  var debug_in: ConsoleReader = new ConsoleReader();
  var debug_out: PrintStream = new PrintStream(System.out);


  val stdio = new ConsoleReader()
  val history_path:String = System.getProperty("user.home") + File.separator + ".ppl_hist";
  val history = new FileHistory(new File(history_path))
  stdio.setHistory(history) 
  stdio.setExpandEvents(false)

  def readLine(prompt: String): String = {
    IO.stdio.readLine(prompt)
  }

  def debug_listen(port_no: Int) {
     debug_disconnect
     debug_ss = new ServerSocket(port_no,0,InetAddress.getLoopbackAddress)
     debug_s = debug_ss.accept()
     debug_out = new PrintStream(debug_s.getOutputStream)
     debug_in = new ConsoleReader(debug_s.getInputStream,debug_s.getOutputStream)
  }

  def debug_disconnect {
      if (null != debug_ss) 
      {
        debug_ss.close()
        debug_ss = null
      }
      if (null != debug_s) 
      {
        debug_s.close()
        debug_s = null
      }
      debug_in = new ConsoleReader();
      debug_out = new PrintStream(System.out);
  }

  def debug_println(s: Any) {
    if (null != debug_out)
    {
       debug_out.println(s.toString)
    }
  }

   def debug_print(s: Any) {
    if (null != debug_out)
    {
       debug_out.print(s.toString)
    }
  }

  def debug_ready() = (debug_in.getInput.available() != 0)
  
  def debug_readLine(prompt: String): String = {
    if (null != debug_in)
    {
       if (null != debug_out) debug_out.print(prompt)
       debug_in.readLine()
    }
    else
       null
  }

  def println(s: Any) {
    Console.println(s.toString)
    //Console.flush()
  }

  def print(s: Any) {
    Console.print(s.toString)
  }

  def errmes(mes: String, culprit: Term, p: Prog) = {
    IO.println("*** " + mes + " in => " + culprit)
    p.stop()
    0
  }

  def warnmes(mes: String) = {
    IO.println("*** " + mes)
    0
  }

}

