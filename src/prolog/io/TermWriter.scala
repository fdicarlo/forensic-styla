package prolog.io
import prolog.terms._
import java.io._
import prolog.forensic._
import scala.language.reflectiveCalls

class TermWriter(val fname: String) extends TermSink {

  val maxBufferSize = 50000000; // Maximal buffer size when writing out contents of a large file

  val out =
    if ("stdio" == fname) scala.Console.out
    else {
      new File(fname).getParentFile.mkdirs();
      new PrintWriter(new OutputStreamWriter( new FileOutputStream(fname), "ISO-8859-1"));  //"UTF-8"));
    }

  override def putElement(x: Term) = {
    if (x.isInstanceOf[TSKContent])
    {
       val content: TSKContent = x.asInstanceOf[TSKContent];
       val contentSize: Long = content.getSize;
       var offset: Long = 0
       var chunkSize: Long = if (contentSize <= maxBufferSize) contentSize else maxBufferSize
       var buf: Array[Byte] = Array.ofDim[Byte](chunkSize.asInstanceOf[Int])
       while (offset < contentSize)
       {
         //IO.println("chunk size: "+chunkSize+" offset: "+offset+" buffer :"+buf)
         content.read(buf,offset,chunkSize);
         out.append(new String(buf, "ISO-8859-1"))
         offset = offset + chunkSize;
         if ( (offset + chunkSize) > contentSize ) 
         { 
            chunkSize = contentSize - offset;
            buf = Array.ofDim[Byte](chunkSize.asInstanceOf[Int])
         }
       } 
       out.flush()
       1
    }
    else
    {
      out.append(x.toString)
      out.flush()
      1
    }
  }

  override def stop() {
    out.close()
  }

  def collect = new Const(fname)

  override def toString = "{" + getClass().getName() + ":" + fname + "}"
}