package prolog.interp

import prolog.fluents.DataBase
import prolog.fluents.Deque
import prolog.io.{IO,TermParser}
import prolog.terms._
import scala.collection.mutable._
import scala.util.matching.Regex


class Prog(val db: DataBase) extends TermSource {
  def this() = this(new DataBase(null))
  def this(fname: String) = this(new DataBase(fname))

  type CLAUSE = List[Term]
  type GOAL = List[Term]

  val trail = new Trail()
  val orStack = new ObjectStack[Unfolder]()
  val copier = new Copier()

  private var answer: Term = null
  private var query: GOAL = null

  var isStopped = true
  private var justBuiltins = false
  var popped = true

  var debuggingModeEngine = false

  def set_query(answer: Term, query: GOAL) {
    orStack.clear()
    trail.clear()

    this.query = query
    this.answer =
      if (!answer.eq(null)) answer
      else {
        // if goal is just a conjunct, this.answer is that conjunct
        val conj: Term = query match {
          case Nil => { stop(); null }
          case head :: Nil => head
          case xs => {
            Conj.fromList(xs)
          }
        }
        conj
      }
   
    val topres = pushUnfolder(query)
    // return from cmd line is ignored

    if (!topres._1.eq(null)) {
      isStopped = false
      justBuiltins = topres._1.isEmpty
    }
  }

  def set_query(query: GOAL) {
    set_query(null, query: GOAL)
  }

  private def pushUnfolder(g0: GOAL): (GOAL, Term) = {

    val g = reduceBuiltins(g0)
    var return_term: Term = null

    if (!g.eq(null) && !g.isEmpty) {
      g.head match {
        // we assume the stub return(_) is defined
        case f: Answer =>
          return_term = f.getArg(0)
        case _ => ()
      }

      val cs = db.getMatches(g)
      if (!cs.eq(null)) {
        val u = new Unfolder(this, g, cs.iterator)
        orStack.push(u)
      }
    }

    (g, return_term)
  }

  private def reduceBuiltins(goal: GOAL): GOAL = {
    if (null == goal) null
    else {
      var newgoal = goal
      if (!newgoal.isEmpty) {
        var ret = 1
        while (ret > 0 && !newgoal.isEmpty) {
          var b: Term = null

          // Step through builtins
          if (Prog.debuggingMode) 
          {
            val next_term = newgoal.head.ref
            if (null != next_term.source_fname)
            {
              if (Prog.continuing) // check if the user pressed STOP or we reached a breakpoint
              {
                IO.debug_println("### Next step: "+next_term.source_fname+"@"+next_term.pos.line)
                Prog.breakpoints.get((next_term.source_fname,next_term.pos.line)) match {
                  case Some(b:Boolean) => if (b) Prog.continuing = false
                  case None => 
                }
                if (IO.debug_ready) Prog.continuing = false  // keep running until some data arrives to the debug stream
              }
              if ((!Prog.continuing) && (next_term.asInstanceOf[Const].sym != (",")) && (next_term.asInstanceOf[Const].sym != (";")))
              {
                 val revMap = db.revVars
  
                 IO.debug_println("### Next Location: "+next_term.source_fname+"@"+next_term.pos);
               
                 IO.debug_println("\nnext goal:\n")
                 val lastTerm = newgoal.last
                 newgoal.foreach
                 {
                   x:Term => //IO.debug_println("   "+x.ref.source_fname+"@"+x.ref.pos+":"+TermParser.term2string(revMap, List(x), if (x == lastTerm) "." else ","))
                            IO.debug_println("   "+TermParser.term2string(revMap, List(x), if (x == lastTerm) "." else ","))
                 }
                 IO.debug_println("\nvariable bindings: \n")
                 trail.foreach
                 {
                   x:Var => 
                   {
                     val varName = revMap.get(x)
                     if (None != varName) 
                        IO.debug_println("   "+varName.get + " = " + x)
                   }  
                 } 
                 processDebugCommand()
              }
            }
          }
          try {
            b = newgoal.head.ref
            ret = b.exec(this)
          } catch {
            case err : Throwable => {
              //println("error=" + err)
              //ret = IO.errmes(err.getLocalizedMessage() +
              ret = IO.errmes(err +
                ",\n*** bad arguments in built-in", b, this)
            }
          }
          if (ret >= 0) newgoal = newgoal.tail
        }
        if (0 == ret) newgoal = null
      }
      newgoal
    }
  }

  override def getElement(): Term = {
    if (isStopped) return null

    if (justBuiltins) {
      stop()
      return answer
    }

    var newgoal: GOAL = null
    var more = true
    while (more && !orStack.isEmpty) {
      val step: Unfolder = orStack.top

      newgoal = step.nextGoal()

      if (step.lastClause && !orStack.isEmpty) {
        orStack.pop()
        popped = true
      } else
        popped = false

      val res = pushUnfolder(newgoal)

      if (null != res._2) return res._2
      newgoal = res._1

      if (Nil == newgoal) more = false
      // "no push when null" eventually stops it but only when orStack.isEmpty
    }

    if (newgoal.eq(null)) {

      if (Prog.debuggingMode && this.debuggingModeEngine)
      {
        IO.debug_println("\n### Finished!")
        Prog.continuing = false
      }
      stop()
      return null
    }

    if (query.isEmpty) stop()

    if (Prog.debuggingMode)
    {
      if (null != answer)
      {
        IO.debug_println("\n### A solution is found!")
      }
    }

    answer
  }

  override def stop() {
    super.stop()
    if (isStopped) trail.unwind(0)
    else {
      //if (null != trail) trail.unwind(0)
      orStack.clear()
      isStopped = true
    }
  }

  val breakSetPattern = """[\s]*b[\s]+([a-zA-Z0-9_\-~\$#\^\./\\]+)@([0-9]+)[\s]*""".r   // b loops@11   -- break in file loops, line 11
  val breakSetPattern2 = """[\s]*b[\s]+([a-zA-Z0-9_\-~\$#\^\.\\]+)/([0-9]+)[\s]*""".r   // b test/1   -- break at every invocation of clause test/1
  val breakListPattern = """[\s]*(b)[\s]*""".r                                          // b            -- list breakpoints
  val breakEnablePattern = """[\s]*b[\s]+on[\s]+([0-9]+)[\s]*""".r                      // b on 1       -- enable breakpoint 1
  val breakDisablePattern = """[\s]*b[\s]+off[\s]+([0-9]+)[\s]*""".r                    // b off 1      -- disable breakpoint 1
  val breakDeletePattern = """[\s]*b[\s]+del([\s]+([0-9]+))?[\s]*""".r                     // b del 1      -- disable breakpoint 1
  val stepPattern = """[\s]*(s)[\s]*""".r                                               // s            -- single step-into
  val nextPattern = """[\s]*(n)[\s]*""".r                                               // n            -- single step-over
  val stopPattern = """[\s]*(quit)[\s]*""".r                                            // quit         -- abort debuggingMode
  val contPattern = """[\s]*(c)[\s]*""".r                                               // c            -- continue execution until next breakpoint
  val stepoutPattern = """[\s]*(o)[\s]*""".r                                            // o            -- step out (until )

  def processDebugCommand() = {
    var cmd: String = null
    var more: Boolean = true
    while(more)
    {
      cmd = IO.debug_readLine(">");
      IO.debug_println(cmd);
      cmd match 
      {    
        case breakSetPattern(file,line) => {
          IO.debug_println("Breakpoint set command: "+file+"@"+line)
          Prog.breakpoints += ((file,line.toInt) -> true)
        }
        case breakSetPattern2(pred,nargs) => {
          val f = new Fun(pred)
          f.init(nargs.toInt)
          db.get(db.key(f)).foreach { 
            clause: Deque[List[Term]] => 
              clause.foreach {
                 t: List[Term] => { 
                   val file = t.head.source_fname;
                   val line = t.head.pos.line;
                   if ((null != file) && (null != line)) {
                     IO.debug_println("Breakpoint set: "+file+"@"+line)
                     Prog.breakpoints += ((file,line) -> true)
                   }
                 }
              }
          }
        }
        case breakListPattern(c) => { 
          IO.debug_println("Breakpoint list command")
          var i:Int = 0
          Prog.breakpoints.foreach { x =>
              IO.debug_println("Breakpoint "+i+": "+x._1._1+"@"+x._1._2+" "+(if(x._2.asInstanceOf[Boolean]) "enabled" else "disabled"))
              i += 1
          }
        }
        case breakEnablePattern(b) => { 
          IO.debug_println("Breakpoint enable command, breakpoint no: " + b)
          val n: Int = b.toInt
          val cnt: Int = Prog.breakpoints.size
          if (n >= 0 && n<cnt)
          {
            Prog.breakpoints.take(n+1).last match
            {
              case ((file: String, line: Int), b: Boolean) => { Prog.breakpoints += ((file,line) -> true) }
            }
          }
        }
        case breakDisablePattern(b) => {  
          IO.debug_println("Breakpoint disable command, breakpoint no: " + b)
          val n: Int = b.toInt
          val cnt: Int = Prog.breakpoints.size
          if (n >= 0 && n<cnt)
          {
            Prog.breakpoints.take(n+1).last match
            {
              case ((file: String, line: Int), b: Boolean) => { Prog.breakpoints += ((file,line) -> false) }
            }
          }
        }
        case breakDeletePattern(opt,b) => {
          if (null != b)
          {
             IO.debug_println("Breakpoint delete command, breakpoint no: " + b)
             val n: Int = b.toInt
             val cnt: Int = Prog.breakpoints.size
             if (n >= 0 && n<cnt)
               Prog.breakpoints = Prog.breakpoints.take(n) ++ Prog.breakpoints.takeRight(cnt-n-1)
          }
          else
          {
             IO.debug_println("Breakpoint delete command: removing all breakpoints")
             Prog.breakpoints.clear
          }
        }
        case stepPattern(c) => {
          IO.debug_println("step-into command")
          more = false
        }
        case nextPattern(c) => {
          IO.debug_println("step-over command") 
          more = false
        }
        case stopPattern(c) => {
          IO.debug_println("STOP debugging command")
          Prog.debuggingMode = false
          Prog.continuing = false
          throw new InterruptedException("Debugging is aborted");
        }   
        case contPattern(c) => {
          IO.debug_println("continue command")
          Prog.continuing = true
          more = false
        }
        case stepoutPattern(c) => {
          IO.debug_println("step-out command")
          more = false
        }
        case _ => IO.debug_println("Unknown command!");
      }
    }
  }
}

object Prog extends Prog {
  
  var debuggingMode: Boolean = false
  var continuing: Boolean = false
  var breakpoints: HashMap[(String,Int),Boolean] = new HashMap[(String,Int),Boolean]()
 
  def make_db(files: Term, p: Prog) = {
    if (Const.nil == files) p.db
    else {
      val file: Term = files.asInstanceOf[Cons].getHead
      val fname: String = file.asInstanceOf[Const].sym
      new DataBase(fname)
    }
  }

  def init_with(db: DataBase, x: Term, g: Term, q: Prog) = {
    val gs0 = x :: Conj.toList(g)
    val gs = Term.copyList(gs0, new Copier())
    q.set_query(gs.head, gs.tail)
  }

  def init_debug_with(db: DataBase, x: Term, g: Term, q: Prog, v: LinkedHashMap[String, Var]) = {
    db.vars ++= v
    val gs = x :: Conj.toList(g)
    q.set_query(gs.head, gs.tail)
  }
}
