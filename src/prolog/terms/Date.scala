package prolog.terms
import java.time._
import java.time.format._

case class Date(nval:Instant) extends Num {

  def this(s: String) = this(Date.parse(s)) 
  def this(n: BigDecimal) = this(Instant.ofEpochSecond(n.toLong))
  def this(n: Int) = this(Instant.ofEpochSecond(n.toLong))

  override def name = Date.format(nval)

  def toBigInt = BigDecimal(nval.getEpochSecond)

  def getValue = BigDecimal(nval.getEpochSecond)

  def toLong = nval.getEpochSecond()

  def getInstant = nval

/*
  override def bind_to(that: Term, trail: Trail) =
    super.bind_to(that, trail) &&
      this.nval == that.asInstanceOf[Instant].nval
*/
}

object Date {
   var outformat:DateTimeFormatter = DateTimeFormatter.ofPattern("MMM dd yyyy HH:mm")
   var informat:DateTimeFormatter = DateTimeFormatter.ofPattern("MMM dd yyyy HH:mm")

   /**
    * Custom parser of date / time literals (t.b.d.)
    */
   def parse( s:String ) : Instant = Instant.parse(s)

   /**
    * Custom formatter of date / time literals (t.b.d.)
    */
   def format( d:Instant ) : String = d.toString

}