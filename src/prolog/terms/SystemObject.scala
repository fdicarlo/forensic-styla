package prolog.terms

abstract class SystemObject extends Nonvar {
  override def name = "{" + getClass().getName() + "}"
}
