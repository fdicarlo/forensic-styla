package prolog.builtins
import prolog.terms._
import prolog.interp.Prog
import scala.util.continuations._
import scala.collection.mutable.ArrayStack
import prolog.io.IO


/*

The following is intended as a possible representation of compiled prolog
predicates like this one

compile_3(N,F) :- 
    eq(N,0),
    eq(F,1).

compile_3(N,F) :- 
    is(1,'?'(N,0)),  % Internal representation of N>0
    is(Np,'-'(N,1)),
    compile_3(Np,Fp),
    is(F,'*'(Fp,N)).

This is compiled into a builtin that creates a term source, whose 
output is then consumed.

*/


final class compile_3() extends FunBuiltin("compile_3", 3) {
   override def exec(p: Prog) = {
      putArg(2,new compile_3_runner(getArg(0),getArg(1),p),p)
   }
}

class compile_3_runner(a0: Term, a1: Term, p: Prog) extends TermSource {

  // Initiate checkpoint stack
  val checkpoints = new ArrayStack[(Term => Term)]();
  checkpoints.push(cldone);
  checkpoints.push(cl2);
  checkpoints.push(cl1);

  def getElement() : Term = checkpoints.top(Const.nil);
 
  def cl1(dummy: Term) : Term = //reset
  {
     checkpoints.pop(); // We are not going to call cl1() again, although new checkpoints may be added in it
     // Function 1: eq(N,0)
     val fn_1 = new prolog.builtins.eq;
     // Initialisation of arguments of Function 1
     val fn_1_args = new Array[Term](2)
     fn_1_args(0) = a0
     fn_1_args(1) = compile_3_runner.const1; // Real(0)
     fn_1.args = fn_1_args
     if (fn_1.exec(p)<1) checkpoints.top(dummy); else {
     // Function 2: eq(F,1)
     val fn_2 = new prolog.builtins.eq;
     // Initialisation of arguments of Function 2
     val fn_2_args = new Array[Term](2)
     fn_2_args(0) = a1
     fn_2_args(1) = compile_3_runner.const2; // Real(1)
     fn_2.args = fn_2_args
     if (fn_2.exec(p)<1) checkpoints.top(dummy); else {
     Const.nil;
     }}
  }

  def cl2(dummy: Term) : Term = //reset 
  {
     checkpoints.pop(); // We are not going to call cl2() again, although new checkpoints may be added in it
     // Function 3: is(1,'?'(N,0))
     val fn_3 = new prolog.builtins.is;
     // Initialisation of arguments of Function 3
     val fn_3_args = new Array[Term](2)
     fn_3_args(0) = compile_3_runner.const2; // Real(1)
       val fn_3_arg_1 = new Fun("?");
       val fn_3_arg_1_args = new Array[Term](2);
       fn_3_arg_1_args(0) = a0;
       fn_3_arg_1_args(1) = compile_3_runner.const1; // Real(0)
       fn_3_arg_1.args = fn_3_arg_1_args
     fn_3_args(1) = fn_3_arg_1
     fn_3.args = fn_3_args
     if (fn_3.exec(p)<1) checkpoints.top(dummy); else {
  
     // Function 4: is(_N,'-'(N,1))
     val fn_4 = new prolog.builtins.is;
     // Initialisation of arguments of Function 4
     val fn_4_args = new Array[Term](2)
     // create new var (_N)
     val v3 = new Var;
     fn_4_args(0) = v3; // _N
       val fn_4_arg_1 = new Fun("-")
       val fn_4_arg_1_args = new Array[Term](2)
       fn_4_arg_1_args(0) = a0 // N
       fn_4_arg_1_args(1) = compile_3_runner.const2; // Real(1)
       fn_4_arg_1.args = fn_4_arg_1_args
     fn_4_args(1) = fn_4_arg_1
     fn_4.args = fn_4_args
     if (fn_4.exec(p)<1) checkpoints.top(dummy); else {
    
     // Function 5 compile_3(_N,_F)
     val v4 = new Var; // _F
     val fn_5 = new compile_3_runner(v3,v4,p); // compile_3(_N,_F)
     //shift[Term,Term,Term] { f: (Term => Term) => /* checkpoints.push(f); */ f(dummy); } // Create a check-point here
     if (null == fn_5.getElement) { /* checkpoints.pop; */ checkpoints.top(dummy); } else {

     // Function 6: is(F,'*'(_N,1))
     val fn_6 = new prolog.builtins.is;
     // Initialisation of arguments of Function 6
     val fn_6_args = new Array[Term](2)
     fn_6_args(0) = a1; // F
       val fn_6_arg_1 = new Fun("*")
       val fn_6_arg_1_args = new Array[Term](2)
       fn_6_arg_1_args(0) = v4; // _F
       fn_6_arg_1_args(1) = a0; // N
       fn_6_arg_1.args = fn_6_arg_1_args
     fn_6_args(1) = fn_6_arg_1
     fn_6.args = fn_6_args
     if (fn_6.exec(p)<1) checkpoints.top(dummy); else {
     Const.nil;
     }}}}
  }

  def cldone(dummy: Term) = { stop(); null }
}

object compile_3_runner {
  // Constants
  val const1 = new prolog.terms.SmallInt(0)
  val const2 = new prolog.terms.SmallInt(1)
}