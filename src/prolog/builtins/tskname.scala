package prolog.builtins
import prolog.terms._
import prolog.interp.Prog
import org.sleuthkit.datamodel._
import collection.JavaConverters._
import prolog.forensic._

final class tskname() extends FunBuiltin("tskname", 2) {

  override def exec(p: Prog) = {
    val datasource:Content = getArg(0).asInstanceOf[TSKContent].c;
    TSKCase.active match {
      case true => {
        putArg(1, 
              new Const(datasource.getName()), 
              p)
      }
      case _ => { -1 }
    }

/**  Examples of how to generate different structures

// From Collection --> Prolog list
    putArg(0, 
           Cons.fromArray(Array(
               new Const(prolog.forensic.TSKCase.fileName), 
               new Const("Ahoy!")             
           )),
           p)

// From List --> Prolog list
    putArg(0, 
           Cons.fromList(List(
               new Const(prolog.forensic.TSKCase.fileName), 
               new Const("Ahoy!")             
           )),
           p)

// N-ary functor.
    putArg(0, 
           new Fun("case",Array(
               new Const(prolog.forensic.TSKCase.fileName), 
               new Const("Ahoy!")             
           )),
           p)    
*/

  }
}
