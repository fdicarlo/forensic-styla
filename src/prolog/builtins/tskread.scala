package prolog.builtins
import prolog.terms._
import prolog.interp.Prog
import org.sleuthkit.datamodel._
import collection.JavaConverters._
import prolog.forensic._

final class tskread() extends FunBuiltin("tskread", 4) {

  override def exec(p: Prog) = {
    val content:TSKContent = getArg(0).asInstanceOf[TSKContent];
    val offset = getArg(1).asInstanceOf[Num].toLong;
    val length = getArg(2).asInstanceOf[Num].toLong;
    TSKCase.active match {
      case true => {
        val buf = Array.ofDim[Byte](length.asInstanceOf[Int])
        if (content.getSize >= offset)
        {
           content.read(buf,offset,length)
           putArg(3, 
                  new Const(new String(buf, "ISO-8859-1")), 
                 p)
        }
        else
           putArg(3,Const.empty,p)
      }
      case _ => { -1 }
    }

  }
}
