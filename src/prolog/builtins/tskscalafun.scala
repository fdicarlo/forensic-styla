package prolog.builtins
import prolog.terms._
import prolog.interp.Prog
import scala.reflect.runtime._
import scala.tools.reflect.ToolBox
import prolog.io._

final class tskscalafun() extends FunBuiltin("tskscalafun", 3) {
  override def exec(p: Prog) = {
        val fname : String = getArg(0).asInstanceOf[Const].name
        val narg = getArg(1).asInstanceOf[Num].toLong
        val code = getArg(2).asInstanceOf[Const].name
        val tree = Tskscalafun.tb.parse(
"import prolog.terms._; import prolog.interp.Prog; final class "+fname+"_builtin() extends FunBuiltin(\""+fname+"_builtin\", "+narg+") { override def exec(p: Prog) = {"+code+"}}; new "+fname+"_builtin")
        //print(tree);
        val f = Tskscalafun.tb.compile(tree)
        val b:FunBuiltin = f().asInstanceOf[FunBuiltin];
        prolog.io.TermParser.builtinMap.put(fname+"_builtin",b.asInstanceOf[Const]);
        val buf = new StringBuilder()
        buf ++= fname
        buf ++= "("
        for (i <- 1 to narg.toInt)
        {
          buf ++= ("_"+i)
          if (i != narg) buf ++=","
        } 
        buf ++= ") :- "+fname+"_builtin("
        for (i <- 1 to narg.toInt)
        {
          buf ++= ("_"+i)
          if (i != narg) buf ++=","
        } 
        buf ++= ")."
        //println(buf.toString)
        val parser = new TermParser(p.db.vars)  
        val cs = parser.parseProg(buf.toString)
        p.db.addAll(cs)
        1
  }
}

object Tskscalafun {
   val cm = universe.runtimeMirror(getClass.getClassLoader)  
   val tb = cm.mkToolBox()
   // tb.parse("text")
   // tb.eval(parse tree)
}