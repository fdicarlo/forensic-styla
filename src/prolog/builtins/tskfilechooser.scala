package prolog.builtins
import prolog.terms._
import prolog.interp.Prog
import org.sleuthkit.datamodel._
import collection.JavaConverters._
import prolog.forensic._
import scala.swing.FileChooser
import java.io.File
import javax.swing.JOptionPane



final class tskfilechooser() extends FunBuiltin("tskfilechooser", 3) {

  override def exec(p: Prog) = {

    val title:String = getArg(0).asInstanceOf[Const].name;
    val opensave = getArg(1).asInstanceOf[Const];
    val fname:String = choosePlainFile(title,opensave)
    fname match {
      case null => { 0 }
      case _ => { putArg(2, new Const(fname), p);  1 }
    }      
  }

  def choosePlainFile(title: String = "", opensave : Const): String = {  
     val chooser = new FileChooser(new File("."))
     chooser.title = title
     val result = if (opensave.isInstanceOf[true_]) chooser.showOpenDialog(null) else chooser.showSaveDialog(null) 
     if (result == FileChooser.Result.Approve) chooser.selectedFile.getCanonicalPath()  else null
  }

}
