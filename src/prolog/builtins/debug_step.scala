package prolog.builtins
import prolog.terms._
import prolog.interp.Prog

final class debug_step() extends FunBuiltin("debug_step", 2) {
  override def exec(p: Prog) = {
    val eval: TermSource = getArg(0).asInstanceOf[TermSource]
    Prog.debuggingMode = true
    val res: Term = eval.getElement()
    Prog.debuggingMode = false
    if (null != res) putArg(1,res,p) else 0
  }
}