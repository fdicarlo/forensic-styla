package prolog.builtins
import prolog.terms._
import prolog.interp.Prog
import org.sleuthkit.datamodel._
import collection.JavaConverters._
import prolog.forensic._
import prolog.io.IO
import java.nio.file._

final class tskmkuniquedir() extends FunBuiltin("tskmkuniquedir", 2) {

  override def exec(p: Prog) = {

    val dirname:String = getArg(0).asInstanceOf[Const].name;
    if (TSKCase.active)
    {
      try {
        putArg(1,new Const(Files.createTempDirectory(Paths.get(TSKCase.getDbDirPath+FileSystems.getDefault().getSeparator()+dirname),"").toString),p)
      } catch {
        case e:Exception  => { IO.warnmes(e.toString); 0 }
      }    
      1  
    }
    else
    {
       IO.warnmes("SleuthkitCase is closed, for tskmkuniquefile() to work the case must be open. New unique file has not been created!");
       0
    }
  }
}
