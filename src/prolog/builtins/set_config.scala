package prolog.builtins
import prolog.terms._
import prolog.interp.Prog
import prolog.io.IO

final class set_config() extends FunBuiltin("set_config", 1) {
  override def exec(p: Prog) = {
    IO.configuration = getArg(0);
    1
  }
}