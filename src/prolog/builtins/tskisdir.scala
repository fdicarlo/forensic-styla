package prolog.builtins
import prolog.terms._
import prolog.interp.Prog
import org.sleuthkit.datamodel._
import collection.JavaConverters._
import prolog.forensic._

final class tskisdir() extends FunBuiltin("tskisdir", 1) {

  override def exec(p: Prog) = {
    val dir:AbstractFile = getArg(0).asInstanceOf[TSKContent].c.asInstanceOf[AbstractFile];
    if (dir.isDir()) 1 else 0
  }
}
