package prolog.builtins
import prolog.terms._
import prolog.interp.Prog

final class neq() extends FunBuiltin("neq", 2) {
  def as(x: Term, y: Term) = {
    args = Array(x, y)
    this
  }

  override def exec(p: Prog) = {
    if (getArg(0).matches(getArg(1),p.trail)) 0 else 1
  }

  override def toString = getArg(0) + "\\=" + getArg(1)
}