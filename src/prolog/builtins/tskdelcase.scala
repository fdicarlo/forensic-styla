package prolog.builtins
import prolog.terms._
import prolog.io._
import prolog.fluents._
import prolog.forensic.TSKCase
import prolog.interp.Prog
import java.io.File

final class tskdelfile() extends FunBuiltin("tskdelfile", 1) {

  override def exec(p: Prog) = {
    val x = getArg(0).asInstanceOf[Const].name
    IO.println("% deleting file " + x)
    val f = new java.io.File(x) 
    if (f.delete) 
    {
      println("% file deleted") 
      1
    }
    else 
    {
      println("% couldn't delete file!")
      0
    }
  }  
}
