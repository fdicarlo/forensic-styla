package prolog.builtins
import prolog.terms._
import prolog.interp.Prog
import prolog.forensic._


final class tskpythoneval() extends FunBuiltin("tskpythoneval", 4) {

  override def exec(p: Prog) = {
        val expr = getArg(0).asInstanceOf[Const].name
        TSKPython.setInteractive(getArg(1).isInstanceOf[true_])
        if (getArg(3).isInstanceOf[true_])
           try
           { 
              TSKPython.eval(expr) 
              putArg(2,new Const(""),p);
           }
           catch { case e : Throwable => putArg(2,new Const(e.getMessage()),p); } 
         else
         {
              TSKPython.eval(expr) 
              putArg(2,new Const(""),p);           
         }
  }
}
