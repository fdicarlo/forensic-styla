package prolog.builtins
import prolog.terms._
import prolog.fluents._
import prolog.interp.Prog
import prolog.io._

final class readline() extends FunBuiltin("readline", 2) {
  override def exec(p: Prog) = {
    val prompt = getArg(0).asInstanceOf[Const].name
    val s = IO.readLine(prompt)
    s match {  
      case line:String => { putArg(1, new Const(line), p) }
      case _ => { putArg(1, new Const(""), p) }
    }
    1
  }
}
