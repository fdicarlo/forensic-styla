package prolog.builtins
import prolog.terms._
import prolog.interp.Prog
import prolog.io.IO

final class read_config() extends FunBuiltin("read_config", 1) {
  override def exec(p: Prog) = {
    IO.read_configuration()
    putArg(0,IO.configuration,p)
    1
  }
}