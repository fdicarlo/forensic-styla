package prolog.builtins
import prolog.terms._
import prolog.io._
import prolog.fluents._
import prolog.forensic._
import prolog.interp.Prog

final class tskaddderivedfile() extends FunBuiltin("tskaddderivedfile", 4) {

  override def exec(p: Prog) = {
    val file_to_add = getArg(0)
    val parent_file = getArg(1).asInstanceOf[TSKContent]
    val derivation = getArg(2).asInstanceOf[Const].sym
    if (TSKCase.active)
    {
       IO.println("% adding derived file/folder " + file_to_add + " as a child of " + parent_file )
       file_to_add match {
         case c: Const => {
           val fname = c.sym
           val df = prolog.forensic.TSKCase.addDerivedFile(fname,parent_file,derivation)
           if (df == 0) 0 else { putArg(3,new TSKContent(df),p); 1}
         }
         case _ => 0
       }
    }
    else
    {
       IO.warnmes("SleuthkitCase is closed. Derived file " + file_to_add + " cannot not be added!");
       0
    }
  }
}
