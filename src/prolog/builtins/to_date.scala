package prolog.builtins
import prolog.terms._
import prolog.interp.Prog

final class to_date() extends FunBuiltin("to_date", 2) {

  override def exec(p: Prog) = {
    getArg(0) match {
       case s : Real => putArg(1, new Date(s.asInstanceOf[Real].toLong), p) 
       case s : SmallInt => putArg(1, new Date(s.asInstanceOf[SmallInt].toLong), p)
       //case s : Fun => putArg(1, new Date(prolog.builtins.is_eval.eval(s)), p)
       case s : Term => putArg(1, new Date(s.toString), p)
    }
  }
}