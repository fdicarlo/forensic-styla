package prolog.builtins
import prolog.terms._
import prolog.interp.Prog
import org.sleuthkit.datamodel._
import collection.JavaConverters._
import prolog.forensic._
import prolog.io.IO
import java.nio.file._

final class tskmkdir() extends FunBuiltin("tskmkdir", 1) {

  override def exec(p: Prog) = {

    val dirname:String = getArg(0).asInstanceOf[Const].name;
    if (TSKCase.active)
    {
      try {
        Files.createDirectories(Paths.get(TSKCase.getDbDirPath+FileSystems.getDefault().getSeparator()+dirname))
      } catch {
        case e:FileAlreadyExistsException  => 1
      }    
      1  
    }
    else
    {
       IO.warnmes("SleuthkitCase is closed, for tskmkdir() to work the case must be open. Case sub-folder \'"+dirname+"\' has not been created!");
       0
    }
  }
}
