package prolog.builtins
import prolog.terms._
import prolog.interp.Prog
import scala.util.continuations._
import scala.collection.mutable.ArrayStack
import prolog.io.IO


/*

The following is intended as a possible representation of compiled prolog
predicates like this one

compile_7(N,F) :- 
    eq(N,0),
    eq(F,1).

compile_7(N,F) :- 
    is(1,'?'(N,0)),  % Internal representation of N>0
    is(Np,'-'(N,1)),
    compile_7(Np,Fp),
    is(F,'*'(Fp,N)).

This is compiled into a builtin that creates a term source, whose 
output is then consumed.

*/


final class compile_7() extends FunBuiltin("compile_7", 3) {
   override def exec(p: Prog) = {
      putArg(2,new compile_7_runner(getArg(0),getArg(1),p),p)
   }
}

class compile_7_runner(a0: Term, a1: Term, p: Prog) extends TermSource {

  // Initiate checkpoint pointer
  var nxt: (()=>Term) = cl1;

  // variables & predicate objects
  var v3 : Var = null;
  var v4 : Var = null;
//  var fn_1 : FunBuiltin = null;
//  var fn_2 : FunBuiltin = null;
//  var fn_3 : FunBuiltin = null;
//  var fn_4 : FunBuiltin = null;
  var fn_5 : compile_7_runner = null;
//  var fn_6 : FunBuiltin = null;

  // Get next solution by starting at the next checkpoint
  def getElement() : Term = nxt();
 
 // Clause 1: compile_7(A0,A1) :- eq(A0,0), eq(A1,1).
  private final def cl2() : Term = 
  {
     // Function 1: eq(N,0)
     val fn_1 = new prolog.builtins.eq;
     fn_1.args = Array[Term](a0,compile_7_runner.const1);
     if (fn_1.exec(p)<1) cldone(); else {
     // Function 2: eq(F,1)
     val fn_2 = new prolog.builtins.eq;
     fn_2.args = Array[Term](a1,compile_7_runner.const2)
	   if (fn_2.exec(p)<1) cldone(); else {
     nxt = cldone;
	   Const.no;
     }}
  }

// Clause 2: compile_7(A0,A1) :- 1 is '?'(A0,0), _N is A0-1, compile_7(_N,_F), A1 is _F*A0.
  private final def cl1() : Term = //reset 
  {
     // Function 3: is(1,'?'(A0,0))  i.e. A0>0
     val fn_3 = new prolog.builtins.is;
     fn_3.args = Array[Term](compile_7_runner.const2,new Fun("?",Array[Term](a0,compile_7_runner.const1)))
     if (fn_3.exec(p)<1) cl2(); else {
  
     // Function 4: is(_N,'-'(N,1))
     val fn_4 = new prolog.builtins.is;
     v3 = new Var; // _N
     fn_4.args = Array[Term](v3,new Fun("-",Array[Term](a0,compile_7_runner.const2)))
     if (fn_4.exec(p)<1) cl2(); else { 
     // Function 5 compile_7(_N,_F)
     v4 = new Var; // _F
     fn_5 = new compile_7_runner(v3,v4,p); // compile_7(_N,_F)
     cl1_contB (); // proceed to invoke the predicate
     }}
  }

  // continuation point for the second clause -- when we search for the next goal
  // other terms are builtin functions and they do not need to be backtracked
  private final def cl1_contA() : Term =
  {
     // this is a repeat invocation of fn_5.
     // unbind any variables bound *below* this point
     // use p.trail?
     // then invoke the predicate again
     cl1_contB();
  }

  private final def cl1_contB() : Term =
  {
     // invoke predicate fn_5 to get next element
     if (null == fn_5.getElement) cl2(); else {
     // Function 6: is(F,'*'(_N,1))
     val fn_6 = new prolog.builtins.is;
     fn_6.args = Array[Term](a1,new Fun("*",Array[Term](v4,a0)))
     if (fn_6.exec(p)<1) cl2(); else {
     nxt = cl1_contA; // If another solution is possible, return to last checkpoint. 
     Const.yes;
     }}
  }

  private final def cldone() : Term = { nxt = cldone; null }
}

object compile_7_runner {
  // Constants
  val const1 = new prolog.terms.SmallInt(0)
  val const2 = new prolog.terms.SmallInt(1)
}