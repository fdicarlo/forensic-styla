package prolog.builtins
import prolog.terms._
import prolog.interp.Prog
import org.sleuthkit.datamodel._
import collection.JavaConverters._
import prolog.forensic._
import scala.swing.FileChooser
import java.io.File
import javax.swing.JOptionPane



final class tskdelconfirm() extends FunBuiltin("tskdelconfirm", 3) {

  override def exec(p: Prog) = {

    val title:String = getArg(0).asInstanceOf[Const].name;
    val fname:String = getArg(1).asInstanceOf[Const].name;
    val n = JOptionPane.showConfirmDialog(
        null,
        "Do you want to destroy existing file\n"+fname+" ?",
        "Existing Case File Destruction",
        JOptionPane.WARNING_MESSAGE)
    putArg (2, new SmallInt(n), p)
    1
  }
}
