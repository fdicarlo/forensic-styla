package prolog.builtins
import prolog.terms._
import prolog.interp.Prog
import org.sleuthkit.datamodel._
import collection.JavaConverters._
import prolog.forensic._
import prolog.io._
import util.matching.Regex
import scala.collection.mutable._
import java.io.ByteArrayInputStream
import scala.sys.process._


final class tskexec() extends FunBuiltin("tskexec", 3) {

  override def exec(p: Prog) = {
    val cmd = getArg(0).asInstanceOf[Const].name
    val indata = getArg(1).asInstanceOf[Const].name
    val in = new ByteArrayInputStream(indata.getBytes("ISO-8859-1"))
    try {
      val out = (cmd #< in).!!
      putArg(2,new Const(out.toString),p)
    } catch {
      case e:Throwable => println(e.getMessage()); putArg(2,new Const(""),p)
    }
    1
  }
}
