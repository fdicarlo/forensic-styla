package prolog.builtins
import prolog.terms._
import prolog.interp.Prog
import org.sleuthkit.datamodel._
import collection.JavaConverters._
import prolog.forensic._
import prolog.io._
import util.matching.Regex
import scala.collection.mutable._

final class writel() extends FunBuiltin("writel", 2) {

   override def exec(p: Prog) = {
     val out:TermWriter = getArg(0).asInstanceOf[TermWriter]
     val data:List[Term] = Cons.toList(getArg(1).asInstanceOf[Cons])
     for (s <- data)
     {
       out.putElement(s)
     }
     1
   }
}