package prolog.builtins
import prolog.terms._
import prolog.io._
import prolog.fluents._
import prolog.interp.Prog

final class use() extends FunBuiltin("use", 1) {

  /*
  ** prefixTerms() post-processes bodies of parsed clauses by prefixing the names of predicates with the given module prefix followewd by __ 
  */
  def prefixTerms(terms: List[Term], p: Prog, prefix: String) : List[Term] =
  {
     terms.map 
     {
        term : Term => term match 
        {
           case f: Fun =>
             if ((prefix == Const.empty) || (f.sym == ".") || (f.isInstanceOf[FunBuiltin]) || (p.db.has_clauses(f) == 1))
             { 
               f
             }
             else if ((f.sym == ",") || (f.sym == ";"))
             {
               val newf = new Fun(f.sym,prefixTerms(f.args.toList,p,prefix).toArray)
               newf.pos = f.pos
               newf.source_fname = f.source_fname
               newf
             }
             else
             { 
               val g = new Fun(prefix+"__"+f.sym)
               g.pos = f.pos
               g.source_fname = f.source_fname
               g.args = f.args
               g
             }
           case _ => term
        }
     }
  }

  override def exec(p: Prog) = {
    val x = getArg(0).ref
    x match {
      case c: Const => {
        val fname = c.sym    
        val f = IO.find_file("modules/"+fname+".pl")
        if (f.eq(null)) 0
        else {
           val parser = new TermParser(p.db.vars)  
           val cs = parser.file2clauses(f)

           // 1. Sift through the clauses looking for module() directive.
           var prefix: String = null;
           var exports: List[Term] = List();
           var module_clause: List[Term] = null;
           for (cl <- cs) cl.head match 
           {
              case f: Fun => 
                if (f.sym == "module")
                {
                  if ((f.args.length == 1) || (f.args.length == 2))
                  {
                     module_clause = cl 
                     prefix = f.args(0).asInstanceOf[Const].sym
                  }
                  if (f.args.length == 2)
                  {
                     exports = Cons.toList(f.args(1))
                  }
                }
              case _ =>  
           }
           if (module_clause == null)
           {
              IO.warnmes("Could not find a well-formed module() declaration in "+fname+" !");
              0
           }
           else if (p.db.has_clauses(new Fun(prefix + "__module",module_clause.head.asInstanceOf[Fun].args)) == 1)
           {
              //IO.warnmes("Module "+prefix+" is already loaded.")
              1
           }
           else
           { 
             // 2. Post-process clause bodies and add them to the database.
             cs.foreach 
             {  
               case cl: List[Term] => 
               {
                 //println(cl);
                 val newh = cl.head match
                 {
                   case hf: Fun => 
                   { 
                      val newhf = new Fun(prefix + "__" + hf.sym, hf.args);
                      newhf.pos = hf.pos
                      newhf.source_fname = hf.source_fname
                      newhf
                   }
                   case hc: Const => 
                      if (hc.sym == "$cmd") 
                        hc 
                      else 
                      {
                        val newhc = new Const(prefix + "__" + hc.sym)
                        newhc.pos = hc.pos
                        newhc.source_fname = hc.source_fname
                        newhc
                      }
                   case _ => cl.head
                 }
                 if (exports.find({ case c : Const => c.sym == cl.head.asInstanceOf[Const].sym; case _ => false }) != None)
                   p.db.add_or_exec(cl.head :: prefixTerms(cl.tail,p,prefix))
                 p.db.add_or_exec(newh :: prefixTerms(cl.tail,p,prefix))   
               }   
             }
             1
           }
        }
      }
      case _ => 0
    }
  }
}
