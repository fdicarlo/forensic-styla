package prolog.builtins
import prolog.terms._
import prolog.interp.Prog
import org.sleuthkit.datamodel._
import collection.JavaConverters._
import prolog.forensic._
import java.util.regex._
import scala.collection.mutable._

final class bin_MATCH() extends FunBuiltin("bin_MATCH", 4) {

  override def exec(p: Prog) = {
    val data_pack = getArg(2).ref.asInstanceOf[Cons]
    val data = data_pack.getHead.asInstanceOf[Const].name
    val pos = data_pack.getBody.asInstanceOf[SmallInt].nval.asInstanceOf[Int]
    val matcher = Pattern.compile(getArg(0).ref.asInstanceOf[Nonvar].name,Pattern.DOTALL).matcher(data)
    matcher.region(pos,data.length)
    if(matcher.lookingAt)
    {
        var groups = new ListBuffer[Term]()
        groups.clear();
        for ( i <- 0 to matcher.groupCount())
        {
            groups += new Const(matcher.group(i))
        }
        getArg(1).ref.unify(Cons.fromList(groups.toList),p.trail)
        putArg(3,new Cons(data_pack.getHead,new SmallInt(matcher.end)),p);
        1
    }
    else
    {
      0
    }
  }
}
