package prolog.builtins
import prolog.terms._
import prolog.interp.Prog


/*

The following is intended as a possible representation of compiled prolog
predicates that contain only builtins.

compile_1(X,Regex) :- type_of(X,'atom'),tskmatches(X,Regex,true).

*/

final class compile_1() extends FunBuiltin("compile_1", 2) {

  // Function arguments
  val v1 = new Var()
  val v2 = new Var()

  // Function 1
  val fn_type_of_1 = new prolog.builtins.type_of;
  // Initialisation of arguments of Function 1
  val fn_type_of_1_args = new Array[Term](2)
  fn_type_of_1_args(0) = v1
  fn_type_of_1_args(1) = new Const("atom")
  fn_type_of_1.args = fn_type_of_1_args

  // Function 2
  val fn_tskmatches_1 = new prolog.builtins.tskmatches;
  // Initialisation of arguments of Function 2
  val fn_tskmatches_1_args = new Array[Term](3)
  fn_tskmatches_1_args(0) = v1
  fn_tskmatches_1_args(1) = v2
  fn_tskmatches_1_args(2) = new true_()
  fn_tskmatches_1.args = fn_tskmatches_1_args

  override def exec(p: Prog) = {
    // bind variables to argument (unless already bound)
    v1.set_to(getArg(0))//v1.unify(getArg(0),p.trail)
    v2.set_to(getArg(1))//v2.unify(getArg(1),p.trail)
    val fn_type_of_1_res = fn_type_of_1.exec(p)
    if (fn_type_of_1_res>0) 
    {
      fn_tskmatches_1.exec(p)
    } 
    else fn_type_of_1_res
  }
}