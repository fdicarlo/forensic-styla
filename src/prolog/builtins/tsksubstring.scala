package prolog.builtins
import prolog.terms._
import prolog.interp.Prog

final class tsksubstring() extends FunBuiltin("tsksubstring", 4) {

  override def exec(p: Prog) = {
    val s = getArg(0).asInstanceOf[Nonvar].toString
    val p0 = getArg(1).asInstanceOf[Num].toLong.toInt
    val p1 = getArg(2).asInstanceOf[Num].toLong.toInt
    putArg(3, new Const(s.substring(p0,p1)), p)
  }
}