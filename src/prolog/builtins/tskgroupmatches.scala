package prolog.builtins
import prolog.terms._
import prolog.interp.Prog
import org.sleuthkit.datamodel._
import collection.JavaConverters._
import prolog.forensic._
import java.util.regex._
import scala.collection.mutable._

final class tskgroupmatches() extends FunBuiltin("tskgroupmatches", 4) {

  override def exec(p: Prog) = {
    val input:String = getArg(0).asInstanceOf[Const].name;
    val pattern:Pattern = getArg(2).isInstanceOf[true_] match { 
                            case true => { Pattern.compile(getArg(1).asInstanceOf[Const].name,Pattern.DOTALL) }
                            case _ => { Pattern.compile(getArg(1).asInstanceOf[Const].name,Pattern.DOTALL | Pattern.CASE_INSENSITIVE) }
                        }
    val matcher:Matcher = pattern.matcher(input)
    if (matcher.find)
    {
        var matches = new ListBuffer[Term]()
        var groups = new ListBuffer[Term]()
        do
        {
           groups.clear();
           for ( i <- 0 to matcher.groupCount())
           {
               groups += new Const(matcher.group(i))
           }
           matches += Cons.fromList(groups.toList)
        } while (matcher.find);

        putArg(3, 
               Cons.fromList(matches.toList), 
               p)
    }
    else 
    { 
      0 
    }

/**  Examples of how to generate different structures

// From Collection --> Prolog list
    putArg(0, 
           Cons.fromArray(Array(
               new Const(prolog.forensic.TSKCase.fileName), 
               new Const("Ahoy!")             
           )),
           p)

// From List --> Prolog list
    putArg(0, 
           Cons.fromList(List(
               new Const(prolog.forensic.TSKCase.fileName), 
               new Const("Ahoy!")             
           )),
           p)

// N-ary functor.
    putArg(0, 
           new Fun("case",Array(
               new Const(prolog.forensic.TSKCase.fileName), 
               new Const("Ahoy!")             
           )),
           p)    
*/

  }
}
