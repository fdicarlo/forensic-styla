package prolog.builtins
import prolog.terms._
import prolog.interp.Prog
import org.sleuthkit.datamodel._
import collection.JavaConverters._
import prolog.forensic._
import java.nio.ByteBuffer


final class bin_u32le() extends FunBuiltin("bin_u32le", 3) {

  override def exec(p: Prog) = {
    val data_pack = getArg(1).ref.asInstanceOf[Cons]
    val data = data_pack.getHead.asInstanceOf[Const].name
    val pos = data_pack.getBody.asInstanceOf[SmallInt].nval.asInstanceOf[Int]
    if (data.length-pos >=4)
    {
       val d : Array[Byte] = data.substring(pos,pos+4).toCharArray.reverse.map(_.toByte);
       val x : Int = ByteBuffer.wrap(d).getInt;
       if(getArg(0).unify(new SmallInt(if(x<0){x+4294967296L}else{x}),p.trail))
       {    
         putArg(2,new Cons(data_pack.getHead,new SmallInt(pos+4)),p); 
         1
       }
       else
       {
          0
       }   
    }
    else
    {
       0
    }
  }
}
