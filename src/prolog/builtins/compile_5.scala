package prolog.builtins
import prolog.terms._
import prolog.interp.Prog
import scala.util.continuations._
import scala.collection.mutable.ArrayStack
import prolog.io.IO


/*

The following is intended as a possible representation of compiled prolog
predicates like this one

compile_3(N,F) :- 
    eq(N,0),
    eq(F,1).

compile_3(N,F) :- 
    is(1,'?'(N,0)),  % Internal representation of N>0
    is(Np,'-'(N,1)),
    compile_3(Np,Fp),
    is(F,'*'(Fp,N)).

This is compiled into a builtin that creates a term source, whose 
output is then consumed.

*/


final class compile_5() extends FunBuiltin("compile_5", 3) {
   override def exec(p: Prog) = {
      val runner = new compile_5_runner(getArg(0),getArg(1),p)
	  putArg(2,runner,p)
   }
}

class compile_5_runner(a0: Term, a1: Term, p: Prog) extends TermSource {

  // checkpoint stack
  val checkpoints = new ArrayStack[(Term => Term)]();
  checkpoints.push(cldone);   
  checkpoints.push(cl2);
  checkpoints.push(cl1);

  def getElement() : Term = checkpoints.top(Const.nil);
 
  def cl1(dummy: Term) : Term = //reset
  {
     checkpoints.pop(); // We are not going to call cl1() again, although new checkpoints may be added in it

     if (!(a0.unify(compile_5_runner.const1,p.trail))) checkpoints.top(dummy); else {
     
     if (!(a1.unify(compile_5_runner.const2,p.trail))) checkpoints.top(dummy); else {
     Const.nil;
     }}
  }

  def cl2(dummy: Term) : Term = //reset 
  {
     var v3 = new Var; // _N
     var v4 = new Var; // _F
     checkpoints.pop(); // We are not going to call cl2() again, although new checkpoints may be added in it
	 if (!(a0.ref.asInstanceOf[Num].getValue > 0)) { checkpoints.top(dummy); } else {
	 if (!(v3.unify(new Real(a0.ref.asInstanceOf[Num].getValue-1),p.trail))) { checkpoints.top(dummy); } else {
     val fn_5 = new compile_5_runner(v3,v4,p); 
     //shift[Term,Term,Term] { f: (Term => Term) => checkpoints.push(f); f(dummy); } // Create a check-point here
     if (null == fn_5.getElement) { /* checkpoints.pop; */ checkpoints.top(dummy); } else {
     if (!(a1.unify(new Real(a0.ref.asInstanceOf[Num].getValue * v4.ref.asInstanceOf[Num].getValue),p.trail))) { checkpoints.top(dummy); } else {
     Const.nil;
     }}}}
  }

  def cldone(dummy: Term) = { stop(); null }


}

object compile_5_runner {
  // Constants
  val const1 = new prolog.terms.SmallInt(0)
  val const2 = new prolog.terms.SmallInt(1)
}