package prolog.builtins
import prolog.terms._
import prolog.interp.Prog
import prolog.io._
import javax.swing._
import javax.swing.table._
import java.awt.BorderLayout
import java.awt.Component
import java.util.Comparator
import scala.collection.mutable._
import scala.util.control.Breaks._

final class tsktable() extends FunBuiltin("tsktable", 3) {

  override def exec(p: Prog) = {
    val title = getArg(0).asInstanceOf[Const].name
    var colNames = Cons.toList(getArg(1).asInstanceOf[Cons]).map( _ match { case c:Const => c.name; case _ => None })
    val data : ArrayBuffer[Array[Object]] = new ArrayBuffer[Array[Object]]();
    prepData(data,getArg(2).asInstanceOf[Term])

    val frame:JFrame = new JFrame(title)
    val newContentPane = new PredTable(colNames.toArray.asInstanceOf[Array[Object]],
                                       data.toArray.asInstanceOf[Array[Array[Object]]]);
    newContentPane.setOpaque(true);
    frame.setLayout(new BorderLayout());
    frame.add(newContentPane, BorderLayout.CENTER);
 
    //Display the window.
    frame.pack();
    frame.setVisible(true);
    1
  }

  private final def prepData(data:ArrayBuffer[Array[Object]], t: Term) : ArrayBuffer[Array[Object]] = t match {
       case Const.nil => data
       case x: Cons => {
          val res = Cons.toList(x.getHead.asInstanceOf[Cons]);
          data += res.toArray
          prepData(data,x.getBody)
       }
  }
  
  class PredTable(colNames:Array[Object],data:Array[Array[Object]]) extends JPanel {

        val table = new JTable( data, colNames)
        table.setAutoResizeMode(JTable.AUTO_RESIZE_OFF)
        table.setAutoCreateRowSorter(true)

        // Adjust initial column width 
        var preferredWidth: Int = 0
        var cellRenderer:TableCellRenderer = null
        var tableColumn:TableColumn = null
        var c:Component = null
        var maxWidth:Int = 0
        var w:Int = 0
        for (column <- 0 until table.getColumnCount())
        {
           tableColumn = table.getColumnModel().getColumn(column);
           preferredWidth = tableColumn.getMinWidth();
           maxWidth = tableColumn.getMaxWidth();
           for (row <- 0 until table.getRowCount())
           {
              cellRenderer = table.getCellRenderer(row, column);
              c = table.prepareRenderer(cellRenderer, row, column);
              w = c.getPreferredSize().width + table.getIntercellSpacing().width+5;
              preferredWidth = Math.max(preferredWidth, w);
 
              //  We've exceeded the maximum width, no need to check other rows
              if (preferredWidth >= maxWidth)
              {
                  preferredWidth = maxWidth;
                  break
              }
            }
            tableColumn.setPreferredWidth( preferredWidth );
        }


        //Add the scroll pane to this panel.
        val scrollPane = new JScrollPane(table);
        setLayout(new BorderLayout)
        add(scrollPane,BorderLayout.CENTER);
   
        if (table.getRowCount()>0)
        {

           // Modify column sorting behaviour
           val sorter:TableRowSorter[TableModel] = new TableRowSorter[TableModel](table.getModel());
           table.setRowSorter(sorter);

           for (col <- 0 until colNames.size) 
           {
              if (table.getModel.getValueAt(0,col).isInstanceOf[prolog.terms.Real]) 
              {
                 sorter.setComparator(col, new RealComparator() )
              } 
              else 
              {
                  if (table.getModel.getValueAt(0,col).isInstanceOf[prolog.terms.SmallInt]) 
                  {
                      sorter.setComparator(col, new SmallIntComparator())
                  } 
                  else 
                  { 
                    if (table.getModel.getValueAt(0,col).isInstanceOf[prolog.terms.Date]) 
                    {
                        sorter.setComparator(col, new DateComparator())
                    }
                  }
              }   
           }
        }        
}

  class RealComparator() extends Comparator[Real]() {
     override def compare(o1:Real, o2:Real):Int = {
       val v1=o1.getValue;
       val v2=o2.getValue;  
       (v1 > v2) match { 
         case true => {1} 
         case _ => {
            (v1 < v2) match {
                case true => { -1 }
                case _ => { 0 }
            }
         }
       }
     }
  }
  class SmallIntComparator() extends Comparator[SmallInt]() {
     override def compare(o1:SmallInt, o2:SmallInt):Int = {
       val v1=o1.toLong;
       val v2=o2.toLong;  
       (v1 > v2) match { 
         case true => {1} 
         case _ => {
            (v1 < v2) match {
                case true => { -1 }
                case _ => { 0 }
            }
         }
       }
     }
  }

  class DateComparator() extends Comparator[prolog.terms.Date]() {
     override def compare(o1:prolog.terms.Date, o2:prolog.terms.Date):Int = o1.nval.compareTo(o2.nval);
  }

}