package prolog.builtins
import prolog.terms._
import prolog.interp.Prog
import org.sleuthkit.datamodel._
import collection.JavaConverters._
import prolog.forensic._
import java.util.regex._
import scala.collection.mutable._

final class tsksetmatcherhorizon() extends FunBuiltin("tsksetmatchernorizon", 1) {

  override def exec(p: Prog) = {
    TSKMatcher.setDefaultHorizon(getArg(0).asInstanceOf[Num].toLong.toInt);
    1
  }
}
