package prolog.builtins
import prolog.terms._
import prolog.interp.Prog
import org.sleuthkit.datamodel._
import collection.JavaConverters._
import prolog.forensic._

final class tskfindname() extends FunBuiltin("tskfindname", 3) {

  override def exec(p: Prog) = {
    val datasource:Content = getArg(0).asInstanceOf[TSKContent].c;
    val filename:String = getArg(1).asInstanceOf[Const].name;
    TSKCase.active match {
      case true => {
        putArg(2, 
              Cons.fromList(TSKCase.tskcase.findFiles(datasource,filename).asScala.toList.map{ new TSKContent(_).asInstanceOf[Term]} ), 
              p)
      }
      case _ => { -1 }
    }

/**  Examples of how to generate different structures

// From Collection --> Prolog list
    putArg(0, 
           Cons.fromArray(Array(
               new Const(prolog.forensic.TSKCase.fileName), 
               new Const("Ahoy!")             
           )),
           p)

// From List --> Prolog list
    putArg(0, 
           Cons.fromList(List(
               new Const(prolog.forensic.TSKCase.fileName), 
               new Const("Ahoy!")             
           )),
           p)

// N-ary functor.
    putArg(0, 
           new Fun("case",Array(
               new Const(prolog.forensic.TSKCase.fileName), 
               new Const("Ahoy!")             
           )),
           p)    
*/

  }
}
