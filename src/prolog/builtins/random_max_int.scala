package prolog.builtins
import prolog.terms._
import prolog.interp.Prog
import scala.util.Random

final class random_max_int() extends FunBuiltin("random_max_int", 2) {
  override def exec(p: Prog) = {
    val max = getArg(0).toString.toInt
    putArg(1, new SmallInt(prolog_random.r.nextInt(max)), p)
    1
  }
}

object prolog_random {
    val r = new Random()
}