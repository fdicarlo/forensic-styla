package prolog.builtins
import prolog.terms._
import prolog.io._
import prolog.fluents._
import prolog.forensic._
import prolog.interp.Prog

final class tskaddlocal() extends FunBuiltin("tskaddlocal", 3) {

  override def exec(p: Prog) = {
    val x = getArg(0)
    val parent:TSKContent = if (getArg(1).isInstanceOf[TSKContent]) getArg(1).asInstanceOf[TSKContent] else null
    if (TSKCase.active)
    {
       IO.println("% adding local file " + x )
       x match {
         case c: Const => {
           val fname = c.sym
           val lf = prolog.forensic.TSKCase.addLocal(fname,parent)
           if (lf == null) 0 else { putArg(1,new TSKContent(lf),p); 1}
         }
         case _ => 0
       }
    }
    else
    {
       IO.warnmes("SleuthkitCase is closed. Local file " + x + " cannot not be added!");
       0
    }
  }
}
