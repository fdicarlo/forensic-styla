package prolog.builtins
import prolog.terms._
import prolog.interp.Prog
import scala.util.continuations._
import scala.collection.mutable.ArrayStack
import prolog.io.IO


/*

The following is intended as a possible representation of compiled prolog
predicates like this one

compile_3(N,F) :- 
    eq(N,0),
    eq(F,1).

compile_3(N,F) :- 
    is(1,'?'(N,0)),  % Internal representation of N>0
    is(Np,'-'(N,1)),
    compile_3(Np,Fp),
    is(F,'*'(Fp,N)).

This is compiled into a builtin that creates a term source, whose 
output is then consumed.

*/


final class compile_6() extends FunBuiltin("compile_6", 2) {
   def fact(x: BigDecimal) : BigDecimal = 
   { 
       var f : BigDecimal = 1; 
       var m : BigDecimal = 1; 
       do 
       {
          f = f*m; 
          m = m+1; 
       } while (x>=m); 
       f 
   }
   override def exec(p: Prog) = {
      var x: BigDecimal = getArg(0).asInstanceOf[Num].getValue;
	  putArg(1,new Real(fact(x)),p)
   }
}

