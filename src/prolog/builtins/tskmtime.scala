package prolog.builtins
import prolog.terms._
import prolog.interp.Prog
import org.sleuthkit.datamodel._
import collection.JavaConverters._
import prolog.forensic._

final class tskmtime() extends FunBuiltin("tskmtime", 2) {

  override def exec(p: Prog) = {
    val datasource:AbstractFile = getArg(0).asInstanceOf[TSKContent].c.asInstanceOf[AbstractFile];
    TSKCase.active match {
      case true => {
        putArg(1, 
              new Date(datasource.getMtime()), 
              p)
      }
      case _ => { -1 }
    }
  }
}
