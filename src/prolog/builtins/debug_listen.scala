package prolog.builtins
import prolog.terms._
import prolog.interp.Prog
import prolog.io.IO

final class debug_listen() extends FunBuiltin("debug_listen", 1) {
  override def exec(p: Prog) = {
    val port_no: Int = getArg(0).asInstanceOf[Num].toLong.toInt
    IO.debug_listen(port_no)
    1
  }
}