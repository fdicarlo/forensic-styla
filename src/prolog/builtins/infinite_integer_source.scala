package prolog.builtins
import prolog.terms._
import prolog.fluents._
import prolog.interp.Prog

final class infinite_integer_source() extends FunBuiltin("infinite_integer_source", 4) {
  override def exec(p: Prog) = {
    val a = getArg(0).toString.toInt 
    val x = getArg(1).toString.toInt
    val b = getArg(2).toString.toInt
    val s = new InfiniteIntegerSource(a, x, b)
    putArg(3, s, p)
  }
}