package prolog.builtins
import prolog.terms._
import prolog.interp.Prog
import org.sleuthkit.datamodel._
import collection.JavaConverters._
import prolog.forensic._
import java.io.File

final class tskfileexists() extends FunBuiltin("tskfileexists", 1) {

  override def exec(p: Prog) = {

    val fname:String = getArg(0).asInstanceOf[Const].name;
    val f = new java.io.File(fname)
    f.exists match {
      case true => { 1 }
      case _ => { 0 }
    }      
  }
}
