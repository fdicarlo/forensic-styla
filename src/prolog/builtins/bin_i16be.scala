package prolog.builtins
import prolog.terms._
import prolog.interp.Prog
import org.sleuthkit.datamodel._
import collection.JavaConverters._
import prolog.forensic._
import java.nio.ByteBuffer


final class bin_i16be() extends FunBuiltin("bin_i16be", 3) {

  override def exec(p: Prog) = {
    val data_pack = getArg(1).ref.asInstanceOf[Cons]
    val data = data_pack.getHead.asInstanceOf[Const].name
    val pos = data_pack.getBody.asInstanceOf[SmallInt].nval.asInstanceOf[Int]
    if (data.length-pos >=2)
    {
       val d : Array[Byte] = data.substring(pos,pos+2).toCharArray.map(_.toByte);
       if(getArg(0).unify(new SmallInt(ByteBuffer.wrap(d).getShort),p.trail))
       {    
         putArg(2,new Cons(data_pack.getHead,new SmallInt(pos+2)),p); 
         1
       }
       else
       {
          0
       }       
    }
    else
    {
       0
    }
  }
}
