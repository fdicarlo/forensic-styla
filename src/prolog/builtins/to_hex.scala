package prolog.builtins
import prolog.terms._
import prolog.interp.Prog

final class to_hex() extends FunBuiltin("to_hex", 3) {

  override def exec(p: Prog) = {
    val n = getArg(1).asInstanceOf[Num].toLong.toInt
    val s = getArg(0).asInstanceOf[Num].toLong.toHexString.toUpperCase
    val hex = ("0000000000000000"+s).takeRight(n)
    if (getArg(2).ref.unify(new Const(hex), p.trail)) 1 else 0
  }
}