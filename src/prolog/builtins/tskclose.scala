package prolog.builtins
import prolog.terms._
import prolog.io._
import prolog.fluents._
import prolog.forensic.TSKCase
import prolog.interp.Prog

final class tskclose() extends FunBuiltin("tskclose", 1) {

  override def exec(p: Prog) = {
      prolog.forensic.TSKCase.close() 
      putArg(0,Const.yes,p)
      1
  }
}
