package prolog.builtins
import prolog.terms._
import prolog.interp.Prog
import prolog.io.IO

final class write_config() extends FunBuiltin("write_config", 1) {
  override def exec(p: Prog) = {
      val a: Term = getArg(0);
      if (!a.isInstanceOf[Var])
      {
          IO.configuration = a
      }
      IO.write_configuration()
    1
  }
}