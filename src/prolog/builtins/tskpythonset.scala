package prolog.builtins
import prolog.terms._
import prolog.interp.Prog
import prolog.forensic._
import collection.JavaConverters._
import jep._


final class tskpythonset() extends FunBuiltin("tskpythonset", 2) {

  def cons2str(c : Cons) : String =
     Cons.to_string(
      {x => x match 
        {
          case l : Cons => cons2str(l);
          case s : Const => "\\\""+s.asInstanceOf[Const].sym+"\\\"";
          case _ => x.toString 
        }
      },
      c)

  override def exec(p: Prog) = {
        val v = getArg(0).asInstanceOf[Const].name
        getArg(1) match {        
           case n : SmallInt => { TSKPython.set(v,n.getValue); 1 }
           case b : Real => { TSKPython.set(v,b.getValue); 1 } 
           case c : Cons => { TSKPython.eval(v+"=eval(\""+cons2str(c)+"\")"); 1 }  
           case an : Any => { TSKPython.set(v,an.toString); 1 }
        }
  }
}
