# DFIRE Forensic Prolog - Prolog in Scala with forensic extensions #

Pavel Gladyshev,
DigitalFIRE 

This is a work in progress to integrate Sleuthkit library (http://www.sleuthkit.org/sleuthkit) and other forensic tools into a Prolog programming environment.

Styla is a lightweight implementation of a Prolog in Scala developed by Paul Tarau (Please see README.txt for more detail)

Scala is a relatively new compiled programming language for JVM platform which includes many advanced features from functional programming languages and may result in shorter programs. The Sleuthkit library includes API interface bindings for the Java platform. It serves as the basis for the Autopsy forensic browser (http://www.sleuthkit.org/autopsy)Scala can directly use Sleuthkit JNI and access + manipulate case files created by Autopsy.

Prolog is a declarative programming language that can drastically reduce the amount of code that needs to be written by forensic examiners when developing custom tools for particular forensic jobs. To see some sample code look into forensic/ folder.

Prolog is an interpreted programming language. While it can search through a file system and perform signature matching within seconds, more substantial data processing operations can (and should) be written as "builtin" predicates in scala langauge. This is quite easy to do.  All builtins are defined in src/prolog/builtins folder. Just copy one of them to another file and edit it. The compiler will automatically pick it up. All forensic extensions begin with 'tsk' (e.g. tskopencase, tskaddimage, tskimagepaths, etc.).

Forensic Prolog runs on Windows (both 64 and 32 bit versions of Windows 7 and upwards), Mac OS X (64 bit), and Ubuntu (64 bit). 

To use Forensic Prolog you will need to install:

* Oracle Java 8 (download Java Runtime or Java SDK from http://www.oracle.com/technetwork/java/javase/downloads/index.html ). 
* Scala 2.11.8 (download 2.11.8 installer from hhttp://www.scala-lang.org/download/all.html ). 
* git (if you do not have git installed on your system, please download the appropriate installer from https://git-scm.com/ ).

As a development / interaction platform we use Microsoft's Visual Studio Code, because it is lightweight, multiplatform, has integrated terminal, and seamlessly works with git ( https://code.visualstudio.com/ ), but obviously you can use any development environment that supports source code editing and command line interaction with OS shell.

Once the above is installed, clone the content of this git repository locally:

git clone https://bitbucket.org/dfirelabs/forensic-prolog.git

To use Forensic Prolog start a command line interface (shell, Integrated Terminal in VS Code, cmd.exe, etc.) and navigate (using 'cd') to the root of the forensic-prolog folder created by git. 

If you are using VS Code, just open forensic-prolog folder using 'Files' --> 'Open...', then start integrated terminal by 'View' --> 'Integrated Terminal': the integrated terminal will automatically start in the root of the 'forensic-prolog' folder.

Additional platform-specific installation activities:
-----------------------------------------------------

In order to use Python, you may need to install and/or configure Python 2.7 according to the instructions given in the file "Configuring Python for use with DFIRE Forensic Prolog.txt"

In order to use ProbLog (a probabilistic extension of prolog), please follow additional installation instructions given in comments at the beginning of the file problog.pl

Recompiling Forensic Prolog:
---------------------------

To (re-)compile Forensic Prolog type

in Windows CMD.EXE:

compile

in Linux, Mac OS X, and in Windows PowerShell:

./compile

Running forensic Prolog:
-----------------------

To run Forensic Prolog type

in Windows CMD.EXE:

run

in Linux, Mac OS X, and in Windows PowerShell:

./run

To start using Forensic Prolog, read FORENSIC-PROLOG.txt and explore examples in forensic/ folder.

Unless you are an active Prolog developer, you will also need to readup on Prolog. There are many good books and free online tutorials: 

* http://www.learnprolognow.org/
* I. Bratko "Prolog Programming for Artificial Intelligence" book.

If you want to contribute to the development of Forensic Prolog, you will probably need to read-up on Scala, whose online tutorials on www.scala-lang.org are a good starting point.

In order to get a deeper understanding of the interpreter and the language you will want to explore the source code at at some point. it is
all stored under src/ folder.

We will also write some tutorials when we get more time ;)

Forensic Prolog is quite easy to modify and we hope that you will enjoy tinkering with it.